//
// Created by milo on 17/12/2020.
//

#include "KeyFrameReader.h"

#include <iostream>
#include <fstream>
#include <vector>
#include "rapidxml.hpp"
#include "KeyFrame.h"

using namespace std;
using namespace rapidxml;

KeyFrameReader::KeyFrameReader(){

};

vector<KeyFrame*>  KeyFrameReader::read(string name, int currentTime) {

    xml_document<> doc;
    xml_node<> * root_node = NULL;

    ifstream theFile ("./KeyFrameData/" + name);
    vector<char> buffer((istreambuf_iterator<char>(theFile)), istreambuf_iterator<char>());
    buffer.push_back('\0');
    doc.parse<0>(&buffer[0]);

    root_node = doc.first_node("KeyFrameSequence");



    vector<KeyFrame*> keyFrameSequence ;

    for (xml_node<> * keyframe_node = root_node->first_node("KeyFrame"); keyframe_node; keyframe_node = keyframe_node->next_sibling()){
        KeyFrame *keyFrame;

        Offset *pos;
        map<string, float> config;
        int time;

        xml_node<> * position_node = keyframe_node->first_node("Position");


        float x = std::stof(position_node->first_node("X")->value());
        float y = std::stof(position_node->first_node("Y")->value());
        float z = std::stof(position_node->first_node("Z")->value());
        pos = new Offset({x, y, z});

        for(xml_node<> * joint_node = keyframe_node->first_node("Joint"); joint_node; joint_node = joint_node->next_sibling()){
            config[joint_node->first_attribute("joint_name")->value()] = std::stof(joint_node->value());
            if (joint_node == keyframe_node->last_node("Joint")){
                break;
            }
        }

        xml_node<> * time_node = keyframe_node->first_node("Time");
        time = std::stoi(time_node->value());
        time += currentTime;

        keyFrame = new KeyFrame(config, pos, time);

        if(position_node->first_node("Motion") != 0){

            int radius = std::stoi(position_node->first_node("Motion")->first_attribute("radius")->value());
            keyFrame->setRadius(radius);
            keyFrame->setMotion( Motions::CIRCLE );
        }

        if(root_node->first_attribute("type") != 0 ){
            keyFrame->setIsContinue(true);
        }



        keyFrameSequence.push_back(keyFrame);
    }

    return keyFrameSequence;
}