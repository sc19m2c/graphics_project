//
// Created by milo on 17/12/2020.
//

#include "KeyFrame.h"
KeyFrame::KeyFrame(){}

KeyFrame::KeyFrame(map<string, float> config, Offset *pos, int time){
    this->config = config;
    this->pos = pos;
    this->time = time;
}

Offset *KeyFrame::getPos() const {
    return pos;
}

void KeyFrame::setPos(Offset *pos) {
    KeyFrame::pos = pos;
}




const map<string, float> &KeyFrame::getConfig() const {
    return config;
}

void KeyFrame::setConfig(const map<string, float> &config) {
    KeyFrame::config = config;
}

int KeyFrame::getTime() const {
    return time;
}

void KeyFrame::setTime(int time) {
    KeyFrame::time = time;
}

void KeyFrame::print(){
    cout << "time: " << this->time << endl;
    cout << "pos: " << this->pos->offset[0] << " : " << this->pos->offset[0] << " : " << this->pos->offset[0]<< endl;
    cout << "config: " << endl;
    for (auto conf = this->config.begin(); conf != this->config.end(); ++conf){
        cout << conf->first << " : " << conf->second << endl;
    }
}

Motions KeyFrame::getMotion() const {
    return motion;
}

void KeyFrame::setMotion(Motions motion) {
    KeyFrame::motion = motion;
}

int KeyFrame::getRadius() const {
    return radius;
}

void KeyFrame::setRadius(int radius) {
    KeyFrame::radius = radius;
}

bool KeyFrame::isContinue1() const {
    return isContinue;
}

void KeyFrame::setIsContinue(bool isContinue) {
    KeyFrame::isContinue = isContinue;
}
