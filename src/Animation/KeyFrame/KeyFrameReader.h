//
// Created by milo on 17/12/2020.
//

#ifndef COURSEWORK2_KEYFRAMEREADER_H
#define COURSEWORK2_KEYFRAMEREADER_H
#include<iostream>
#include "KeyFrame.h"
#include <vector>
using namespace std;

class KeyFrameReader {
public:
    KeyFrameReader();
    vector<KeyFrame*> read(string filename, int currentTime);
};


#endif //COURSEWORK2_KEYFRAMEREADER_H
