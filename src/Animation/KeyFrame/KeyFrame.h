//
// Created by milo on 17/12/2020.
//

#ifndef COURSEWORK2_KEYFRAME_H
#define COURSEWORK2_KEYFRAME_H

#include<iostream>
#include<map>
#include "../../Types/Offset.h"

enum class Motions { CIRCLE , None };

using namespace std;

enum OffsetType{ RELATIVE, ABSOLUTE };

class KeyFrame {
public:
    KeyFrame();
    KeyFrame(map<string, float> config, Offset *pos, int time);
    void print();
protected:
public:
    Motions getMotion() const;

    void setMotion(Motions motion);

protected:
    Offset *pos;
    Motions motion = Motions::None;
    int radius = 0;
    map<string, float> config;
public:
    bool isContinue1() const;

    void setIsContinue(bool isContinue);

protected:
    bool isContinue;
public:
    Offset *getPos() const;

    void setPos(Offset *pos);

    int getRadius() const;

    void setRadius(int radius);

    OffsetType getOffsetType() const;

    void setOffsetType(OffsetType offsetType);

    const map<string, float> &getConfig() const;

    void setConfig(const map<string, float> &config);

    int getTime() const;

    void setTime(int time);

protected:
    int time;
};


#endif //COURSEWORK2_KEYFRAME_H
