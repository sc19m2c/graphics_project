//
// Created by milo on 17/12/2020.
//

#include "Animation.h"
#include "KeyFrame/KeyFrameReader.h"
#include "../Groups/Register.h"
#include "../InterfaceController.h"
#include <cmath>

Animation::Animation(string name, KeyFrame *startKeyFrame) {
    this->startKeyframe = startKeyFrame;
    this->name = name;
}

map<string, float> Animation::calcCurrentConfig(int currentTime) {

    map<string, float> currentConfig;
    float baseTime = targetKeyFrame->getTime() - startKeyframe->getTime();
    float stepTime = (currentTime - startKeyframe->getTime()) / baseTime;

    for (auto joint = startKeyframe->getConfig().begin(); joint != startKeyframe->getConfig().end(); ++joint) {
        float startAngle = joint->second;
        float targetAngle = targetKeyFrame->getConfig().at(joint->first);

        float currentAngle = (((1.0 - stepTime) * startAngle) + (stepTime * targetAngle));
        currentConfig[joint->first] = currentAngle;

    }

    switch (targetKeyFrame->getMotion()) {
        case Motions::CIRCLE:
            currentConfig["root"] = 90+float(currentTime)/(50.0/this->speed);
    }

    if (currentTime == targetKeyFrame->getTime()) {
        //cout << "here ==========================" << endl;
        this->startKeyframe->setConfig(this->targetKeyFrame->getConfig());
    }

    return currentConfig;
}

Offset * Animation::calcCirlcularXY(int currentTime, int radius) {
    float angle = ((float(currentTime) / (50.0/this->speed)) * 3.14159265359) / 180;
    float x = 1.0 + (float(radius*this->radius) * (sin(angle)));
    float z = 1.0 + (float(radius*this->radius) * (cos(angle)));
    return new Offset ({x,2.5, z});
}


Offset *Animation::calcCurrentPosition(int currentTime) {
    Offset *currentPosition;

    switch (targetKeyFrame->getMotion()) {
        case Motions::CIRCLE:
            currentPosition = calcCirlcularXY(currentTime, targetKeyFrame->getRadius());
            break;
        case Motions::None :
            float baseTime = targetKeyFrame->getTime() - startKeyframe->getTime();
            float stepTime = (currentTime - startKeyframe->getTime()) / baseTime;

            float x = (((1.0 - stepTime) * startKeyframe->getPos()->offset[0]) +
                       (stepTime * targetKeyFrame->getPos()->offset[0]));
            float y = (((1.0 - stepTime) * startKeyframe->getPos()->offset[1]) +
                       (stepTime * targetKeyFrame->getPos()->offset[1]));
            float z = (((1.0 - stepTime) * startKeyframe->getPos()->offset[2]) +
                       (stepTime * targetKeyFrame->getPos()->offset[2]));

            currentPosition = new Offset({x, y, z});

            if (currentTime == targetKeyFrame->getTime()) {
                this->startKeyframe->setPos(this->targetKeyFrame->getPos());
            }
            break;
    }


    return currentPosition;
}

void Animation::loadKeyFrameSequence(string name, int currentTime) {
    this->filename = name;
    this->isActivate = true;
    this->startKeyframe->setTime(currentTime);
    this->keyIndex = 0;
    KeyFrameReader *reader = new KeyFrameReader();
    this->keyFrameSequence = reader->read(name, currentTime);
    this->targetKeyFrame = keyFrameSequence[this->keyIndex];
}

void Animation::animate(int currentTime, Register *reg, InterfaceController *interface) {

    if (!this->isActivate) {
        return;
    }

    map<string, float> currentConfig = calcCurrentConfig(currentTime);
    reg->setRobotConfig(this->name, currentConfig);
    reg->setRobotPosition(this->name, calcCurrentPosition(currentTime));
    interface->update();


    if (currentTime == targetKeyFrame->getTime()) {
        keyIndex++;
        if (this->targetKeyFrame == keyFrameSequence.back()) {
            if(this->targetKeyFrame->isContinue1()){
                loadKeyFrameSequence(filename, currentTime);
                return;
            }
            this->isActivate = false;
            return;
        }
        this->startKeyframe->setTime(currentTime);
        this->targetKeyFrame = keyFrameSequence[keyIndex];
    }



}

KeyFrame *Animation::getStartKeyframe() const {
    return startKeyframe;
}

void Animation::setStartKeyframe(KeyFrame *startKeyframe) {
    Animation::startKeyframe = startKeyframe;
}

KeyFrame *Animation::getTargetKeyFrame() const {
    return targetKeyFrame;
}

void Animation::setTargetKeyFrame(KeyFrame *targetKeyFrame) {
    Animation::targetKeyFrame = targetKeyFrame;
}

const list<string *> &Animation::getPaths() const {
    return paths;
}

void Animation::setPaths(const list<string *> &paths) {
    Animation::paths = paths;
}

const vector<KeyFrame *> &Animation::getKeyFrameSequence() const {
    return keyFrameSequence;
}

void Animation::setKeyFrameSequence(const vector<KeyFrame *> &keyFrameSequence) {
    Animation::keyFrameSequence = keyFrameSequence;
}

const string &Animation::getName() const {
    return name;
}

void Animation::setName(const string &name) {
    Animation::name = name;
}

double Animation::getSpeed() const {
    return speed;
}

void Animation::setSpeed(double speed) {
    Animation::speed = speed;
}

double Animation::getRadius() const {
    return radius;
}

void Animation::setRadius(double radius) {
    Animation::radius = radius;
}





