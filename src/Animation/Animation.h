//
// Created by milo on 17/12/2020.
//

#ifndef COURSEWORK2_ANIMATION_H
#define COURSEWORK2_ANIMATION_H

#include<iostream>
#include<map>
#include<list>
#include<vector>
#include "../Types/Offset.h"
#include "KeyFrame/KeyFrame.h"


using namespace std;
class Register;
class InterfaceController;

class Animation {

protected:
    string name;
    string filename;
    KeyFrame *startKeyframe;
    double speed = 1.0;
    double radius =1.0;
public:
    const string &getName() const;

    double getSpeed() const;

    void setSpeed(double speed);

    double getRadius() const;

    void setRadius(double radius);

    void setName(const string &name);

protected:
    KeyFrame *targetKeyFrame;

    vector<KeyFrame*> keyFrameSequence;

    bool isActivate = false;
    int keyIndex = 0;
    list<string*> paths;


public:
    KeyFrame *getStartKeyframe() const;
    void setStartKeyframe(KeyFrame *startKeyframe);
    KeyFrame *getTargetKeyFrame() const;
    const list<string *> &getPaths() const;
    void setPaths(const list<string *> &paths);

    const vector<KeyFrame *> &getKeyFrameSequence() const;

    void setKeyFrameSequence(const vector<KeyFrame *> &keyFrameSequence);

    void setTargetKeyFrame(KeyFrame *targetKeyFrame);

    Offset * calcCirlcularXY(int currentTime, int radius);

public:
    Animation(string name,KeyFrame *startKeyFrame);
    map<string,float> calcCurrentConfig(int currentTime);
    Offset* calcCurrentPosition(int currentTime);

    void loadKeyFrameSequence(string name,int currentTime); // read the keyframes from files -> set initial targetKeyframe
    void animate(int currentTime, Register *r, InterfaceController *interface); // set the register -> update the interface -> set isCOmplete
};


#endif //COURSEWORK2_ANIMATION_H
