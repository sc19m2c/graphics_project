set(FIND_RAPIDXML_PATHS
        ~/Documents/leeds_uni/comp_graphics/coursework2/libs/rxml)

find_path(RAPIDXML_INCLUDE_DIR rapidxml.hpp
        PATH_SUFFIXES include
        PATHS ${FIND_RAPIDXML_PATHS})

find_library(RAPID_XML_LIBRARY
        NAMES rxml
        PATH_SUFFIXES lib
        PATHS ${FIND_RAPIDXML_PATHS})