#include <iostream>
#include <QApplication>
#include <QVBoxLayout>
#include "Window.h"
#include "UI/MasterLayout.h"

int main(int argc, char *argv[]) {
    // create the application
    QApplication app(argc, argv);
    Window *window = new Window(NULL);
    window->resize(1000, 700);
    window->show();
    app.exec();
    // clean up
    //	delete controller;
    delete window;

    // return to caller
    return 0;
}
