//
// Created by milo on 30/11/2020.
//

#ifndef COURSEWORK2_INTERFACECONTROLLER_H
#define COURSEWORK2_INTERFACECONTROLLER_H
#include <QGLWidget>
#include <QObject>
#include "3D_Shapes/Cube.h"
#include "Groups/Register.h"
#include "Camera/Camera.h"

struct WorldSpace {
    float x;
    float y;
    float z;
};

struct AppState{
    bool isTextures;
    bool isShadows;
    bool isClipShadows;
    bool isCubemap;
    bool isWorldmap;
    bool isHumanoid;
    bool isCenter;
};


class InterfaceController : public QGLWidget { //
Q_OBJECT
public slots:
    // called by the slider in the main window
    void setGLU_x(int);
    void update_positions();

public:
    InterfaceController(QWidget *parent);
    ~InterfaceController();

    int getCurrentTime() const;
    void setCurrentTime(int currentTime);

    Register *getReg() const;
    void setReg(Register *reg);
    int getCounter() const;
    // -------------------- requires implimentation ---------------/
    void resetInterface();

protected:
    // called when OpenGL context is set up
    void initializeGL();
    void setLighting();
    void projectShadows();

    void drawSceneBoundary();
    void drawSceneInterior(bool isShadow = false);

    void clipShadows();

    // called every time the widget is resized
    void resizeGL(int w, int h);

    // called every time the widget needs painting
    void paintGL();


    GLfloat projectionMatrix[16];
    GLfloat projectionMatrixLeft[16];
    GLfloat projectionMatrixRight[16];
    GLfloat projectionMatrixFront[16];
    GLfloat light_pos[4] = {};
    WorldSpace space;
    AppState state = {
            true,
            true,
            true,
            true,
            true,
            true,
            true,
    };
public:
    const AppState &getState() const;

    void setState(const AppState &state);

protected:

    int currentTime = 0;

    Register *reg;
    Camera *cam;
    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void keyPressEvent(QKeyEvent *event);

private:
    struct OrthoPar {
        float _x_min;
        float _x_max;
        float _y_min;
        float _y_max;
        float _z_min;
        float _z_max;

        OrthoPar(float x_min,
                 float x_max,
                 float y_min,
                 float y_max,
                 float z_min,
                 float z_max) :
                _x_min(x_min),
                _x_max(x_max),
                _y_min(y_min),
                _y_max(y_max),
                _z_min(z_min),
                _z_max(z_max) {
        }

    };
    struct GluPar {
        float _x;
        float _y;
        float _z;
        float _at_x;
        float _at_y;
        float _at_z;
        float _up_x;
        float _up_y;
        float _up_z;

        GluPar(float x,
               float y,
               float z,
               float at_x,
               float at_y,
               float at_z,
               float up_x,
               float up_y,
               float up_z) :
                _x(x),
                _y(y),
                _z(z),
                _at_x(at_x),
                _at_y(at_y),
                _at_z(at_z),
                _up_x(up_x),
                _up_y(up_y),
                _up_z(up_z) {
        }

    };

    OrthoPar _ortho_par;
    GluPar _glupar;
    bool _b_lighting;

};


#endif //COURSEWORK2_INTERFACECONTROLLER_H
