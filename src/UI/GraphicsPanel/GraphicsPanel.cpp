//
// Created by milo on 15/12/2020.
//

#include <iostream>
#include "GraphicsPanel.h"

using namespace std;

void GraphicsPanel::setGeometry(const QRect &r){
    QLayout::setGeometry(r);
    interface->setGeometry(r);
}



void GraphicsPanel::createWidgets(){
    this->interface = new InterfaceController(this->app);
    addWidget(interface);
}