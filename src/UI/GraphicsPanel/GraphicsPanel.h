//
// Created by milo on 15/12/2020.
//

#ifndef COURSEWORK2_GRAPHICSPANEL_H
#define COURSEWORK2_GRAPHICSPANEL_H


#include "../MasterLayout.h"
#include "../../InterfaceController.h"

class GraphicsPanel : public MasterLayout {
public:
    GraphicsPanel(Window *app) : MasterLayout(app) {};

    void setGeometry(const QRect &rect);

    void createWidgets();

    // beneath that, the main widget
    InterfaceController *interface;

};


#endif //COURSEWORK2_GRAPHICSPANEL_H
