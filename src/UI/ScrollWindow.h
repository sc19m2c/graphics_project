//
// Created by milo on 22/12/2020.
//

#ifndef COURSEWORK2_SCROLLWINDOW_H
#define COURSEWORK2_SCROLLWINDOW_H


#include <QWidget>
#include "MasterLayout.h"

class ScrollWindow : public QWidget {

public:
    ScrollWindow();
    void createWidgets(const QRect &r);
    MasterLayout *getCatScroll();

private:

    MasterLayout *scrollLayout;
};


#endif //COURSEWORK2_SCROLLWINDOW_H
