//
// Created by milo on 15/12/2020.
//

#include <iostream>
#include <QLabel>
#include <QScrollArea>
#include "ControlPanel.h"
#include "../ScrollWindow.h"
#include "LightingControl/LightingControl.h"

ControlPanel::ControlPanel(Window *app): MasterLayout(app){
    this->interface = this->app->getAnInterface();
}

void ControlPanel::setScroller(ScrollWindow *container, const QRect &r, float y){

    //defines the geormetry dependig on the width of the page

    container->setGeometry(QRect(r.x()+10,y,r.width()-20,250));

    container->show();
    QList<QObject*> inside = container->children();

    // loop threough scroll windows children to find the scroll area
    for (int i = 0 ; i < inside.length() ; i ++){
        QScrollArea* scroll = dynamic_cast<QScrollArea*>(inside[i]);
        if (scroll){
            scroll->widget()->setMinimumSize(r.width()-40, 1000);

//            scroll->setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
            scroll->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff);
            scroll->setWidgetResizable( true );

            scroll->setMinimumSize(r.width()-20, 250);
            scroll->setGeometry(QRect(0,0,container->sizeHint().width()-40,250));
            continue;

        }
        QLabel* frame = dynamic_cast<QLabel*>(list_.at(i)->widget());
        if (frame){
            cout << "============================" << endl;
        }
    }
}

void ControlPanel::setGeometry(const QRect &r){

    QLayout::setGeometry(r);

    for (int i = 0; i < list_.size(); i++) {

        QLabel* frame = dynamic_cast<QLabel*>(list_.at(i)->widget());
        if(frame){
            if (frame->text().contains("Animation Controls")){
                frame->setGeometry(QRect(r.x()+10,r.y()+(10*(i))+260,r.width()-20,100));
                frame->layout()->setGeometry(QRect(0,0,r.width()-20,100));
                continue;
            }
            if (frame->text().contains("Lighting Controls")){
                frame->setGeometry(QRect(r.x()+10,r.y()+(10*(i))+360,r.width()-20,200));
                frame->layout()->setGeometry(QRect(0,0,r.width()-20,200));
                continue;
            }
        }
        ScrollWindow* container = dynamic_cast<ScrollWindow*>(list_.at(i)->widget());
        if(container){
            setScroller(container,r,r.y()+(10*(i)+10));
            continue;
        }

    }
}



void ControlPanel::createWidgets(){

    QString myStyle = QString( "QFrame {"
                               "border: 2px solid #c1c1c1;"
                               "border-radius: 4px;"
                               "}");
    QWidget *rc_frame = new QWidget();
    rc_frame->setStyleSheet(myStyle);
    rcLayout = new RobotControlLayout(this->app);
    rcLayout->createWidgets();
    rc_frame->setLayout(rcLayout);


    ScrollWindow * scrollWin = new ScrollWindow();
    QScrollArea *scroll = new QScrollArea(scrollWin);
    scroll->setStyleSheet(myStyle);
    scroll->setWidget(rc_frame);

    addWidget(scrollWin);



    QLabel *ac_frame = new QLabel("Animation Controls");
    ac_frame->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    ac_frame->setStyleSheet(myStyle);
    acLayout = new AnimationControlLayout(this->app);

    acLayout->createWidgets();
    ac_frame->setLayout(acLayout);
    addWidget(ac_frame);

    QLabel *lc_frame = new QLabel("Lighting Controls");
    lc_frame->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    lc_frame->setStyleSheet(myStyle);
    lcLayout = new LightingControl(this->app, this->interface);


    lcLayout->createWidgets();
    lc_frame->setLayout(lcLayout);
    addWidget(lc_frame);

}