//
// Created by milo on 30/12/2020.
//

#include "LightingControl.h"
#include "../../../3D_Shapes/Textures/Textures.h"

LightingControl::LightingControl(Window *app, InterfaceController *interface): MasterLayout(app){
    this->interface = interface;
}

void LightingControl::setGeometry(const QRect &r){
    QLayout::setGeometry(r);


    for (int i = 0; i < list_.size(); i++) {


        QCheckBox* button = dynamic_cast<QCheckBox*>(list_.at(i)->widget());
        if (button){
            if (i % 2) {
                /* x is odd */
                button->setGeometry(QRect(r.x()+10+(r.width()/2),r.y()+(20*(i-1))+30,(r.width()/2)-10,20));
            }else{
                button->setGeometry(QRect(r.x()+10,r.y()+(20*(i))+30,(r.width()/2)-10,20));
            }


            continue;
        }

    }

}


void LightingControl::createWidgets(){


    cubemap = new QCheckBox("Cubemap");
    cubemap->setCheckState(Qt::Checked);
    connect(cubemap, SIGNAL(toggled( bool )),  this, SLOT(setIsCubemap(bool )));
    addWidget(cubemap);

    worldmap = new QCheckBox("World Map");
    worldmap->setCheckState(Qt::Checked);
    connect(worldmap, SIGNAL(toggled( bool )),  this, SLOT(setIsWordlmap(bool )));
    addWidget(worldmap);

    center = new QCheckBox("Center Piece");
    center->setCheckState(Qt::Checked);
    connect(center, SIGNAL(toggled( bool )),  this, SLOT(setIsCenter(bool )));
    addWidget(center);

    humanoid = new QCheckBox("Humanoid");
    humanoid->setCheckState(Qt::Checked);
    connect(humanoid, SIGNAL(toggled( bool )),  this, SLOT(setIsHumanoid(bool )));
    addWidget(humanoid);

    textures = new QCheckBox("Textures");
    textures->setCheckState(Qt::Checked);
    connect(textures, SIGNAL(toggled( bool )),  this, SLOT(setIsTextures(bool )));
    addWidget(textures);

    shadows = new QCheckBox("Shadows");
    shadows->setCheckState(Qt::Checked);
    connect(shadows, SIGNAL(toggled( bool )),  this, SLOT(setIsShadows(bool )));
    addWidget(shadows);

    clipShadows = new QCheckBox("Clipped shadows");
    clipShadows->setCheckState(Qt::Checked);
    connect(clipShadows, SIGNAL(toggled( bool )),  this, SLOT(setIsClipped(bool )));
    addWidget(clipShadows);


}

void LightingControl::setIsTextures(bool isTextures) {
    Textures::setIsTextures(isTextures);
    AppState currentState = this->interface->getState();
    currentState.isTextures = isTextures;
    this->interface->setState(currentState);
    this->interface->update();
}

void LightingControl::setIsCenter(bool isCenter)  {
    AppState currentState = this->interface->getState();
    currentState.isCenter = isCenter;
    this->interface->setState(currentState);
    this->interface->update();
}
void LightingControl::setIsClipped(bool isClipped)  {
    AppState currentState = this->interface->getState();
    currentState.isClipShadows = isClipped;
    this->interface->setState(currentState);
    this->interface->update();
}
void LightingControl::setIsCubemap(bool isCubemap)  {
    AppState currentState = this->interface->getState();
    currentState.isCubemap = isCubemap;
    this->interface->setState(currentState);
    this->interface->update();

}
void LightingControl::setIsHumanoid(bool isHumanoid)  {
    AppState currentState = this->interface->getState();
    currentState.isHumanoid = isHumanoid;
    this->interface->setState(currentState);
    this->interface->update();
}
void LightingControl::setIsShadows(bool isShadows)  {
    AppState currentState = this->interface->getState();
    currentState.isShadows = isShadows;
    this->interface->setState(currentState);
    this->interface->update();
}
void LightingControl::setIsWordlmap(bool isWorldmap)  {
    AppState currentState = this->interface->getState();
    currentState.isWorldmap = isWorldmap;
    this->interface->setState(currentState);
    this->interface->update();
}