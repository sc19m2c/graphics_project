//
// Created by milo on 30/12/2020.
//

#ifndef COURSEWORK2_LIGHTINGCONTROL_H
#define COURSEWORK2_LIGHTINGCONTROL_H


#include <QPushButton>
#include <QCheckBox>
#include "../../MasterLayout.h"

class LightingControl : public MasterLayout {
    Q_OBJECT
public:
    LightingControl(Window *app, InterfaceController *interface);

    void setGeometry(const QRect &rect);

    void createWidgets();

    // and a slider for the number of vertices
protected:
    InterfaceController *interface;
    QCheckBox *textures;
    QCheckBox *shadows;
    QCheckBox *clipShadows;
    QCheckBox *cubemap;
    QCheckBox *worldmap;
    QCheckBox *humanoid;
    QCheckBox *center;

public slots:
    void setIsTextures(bool isTextures);
    void setIsShadows(bool);
    void setIsClipped(bool);
    void setIsCubemap(bool);
    void setIsWordlmap(bool);
    void setIsHumanoid(bool);
    void setIsCenter(bool);
};


#endif //COURSEWORK2_LIGHTINGCONTROL_H
