//
// Created by milo on 16/12/2020.
//

#include <iostream>
#include "ButtonLayout.h"


ButtonLayout::ButtonLayout(Window *app, string robotName, string name, float angle): MasterLayout(app){
    this->name = name;
    this->angle = angle;
    this->robotName = robotName;
    this->interface = this->app->getAnInterface();
    this->reg = this->interface->getReg();

}

void ButtonLayout::setGeometry(const QRect &r){

    QLayout::setGeometry(r);

    for (int i = 0; i < list_.size(); i++) {
        QLabel* label = dynamic_cast<QLabel*>(list_.at(i)->widget());
        if (label){
            label->setGeometry(QRect(r.x()+10,r.y(),(r.width()/2)-30,r.height()));
            continue;
        }
        QDoubleSpinBox* componentAngle = dynamic_cast<QDoubleSpinBox*>(list_.at(i)->widget());
        if (componentAngle){
            componentAngle->setGeometry(QRect(r.width()/2,r.y(),(r.width()/2)-30,r.height()));
            continue;
        }
    }

}


void ButtonLayout::createWidgets(){


    label = new QLabel(QString::fromStdString(this->name));
    addWidget(label);

    componentAngle = new QDoubleSpinBox();
    componentAngle->setRange(-360.0, 360.0);
    componentAngle->setValue(angle);
    componentAngle->setSingleStep(1.);
    connect(componentAngle, SIGNAL(valueChanged(const double &)),  this, SLOT(setAngle(const double &)));
    addWidget(componentAngle);

}

void ButtonLayout::setAngle(double d) {
    cout << this->name << " angle change to :: " << d << endl;
    map<string, float> conf = this->reg->getRobotConfig(this->robotName);
    conf[this->name] = d;
    this->reg->setRobotConfig(this->robotName, conf);
    this->interface->update();
    this->update();
}

const string &ButtonLayout::getRobotName() const {
    return robotName;
}

void ButtonLayout::setRobotName(const string &robotName) {
    ButtonLayout::robotName = robotName;
}
