//
// Created by milo on 16/12/2020.
//

#include <QPushButton>
#include <iostream>
#include "RobotControlLayout.h"
#include "ButtonLayout.h"
#include <iostream>

using namespace std;

RobotControlLayout::RobotControlLayout(Window *app): MasterLayout(app){
    this->reg = this->app->getAnInterface()->getReg();
}

void RobotControlLayout::setGeometry(const QRect &r){
    QLayout::setGeometry(r);

    int c = 1;
    for (int i = 0; i < list_.size(); i++) {

        QPushButton* refresh = dynamic_cast<QPushButton*>(list_.at(i)->widget());
        if(refresh){
            cout << "here" << endl;
            refresh->setGeometry(QRect(r.width()-110,r.y()+5, 100, 30));
            continue;
        }
        QLabel* label = dynamic_cast<QLabel*>(list_.at(i)->widget());
        if(label){
            cout << "here 2" << endl;
            label->setGeometry(QRect(r.x()+10,r.y()+5, 100, 30));

            continue;
        }

        QLayoutItem *o = list_.at(i);
        o->setGeometry(QRect(r.x()+10,r.y()+30, r.width()-20, 30));

        ButtonLayout* button = dynamic_cast<ButtonLayout*>(list_.at(i));
        if (button){

            if(button->getRobotName().compare(this->robotSelection->currentText().toStdString()) == 0){

                button->setGeometry(QRect(r.x()+10,r.y()+(30*(c+1)),r.width(),30));
                c++;
            }
            else{
                button->setGeometry(QRect(0,0,0,0));
            }
            continue;
        }

    }

}


void RobotControlLayout::createWidgets(){


    robotSelection = new QComboBox();
//    robotSelection->addItem("robot 1");
    connect(robotSelection, SIGNAL(currentTextChanged(const QString &)),  this, SLOT(selectRobot()));
    addWidget(robotSelection);


//    QLabel *label = new QLabel("Robot Controls");
//    addWidget(label);
//
//    QPushButton *refresh = new QPushButton("refresh");
//    connect(refresh, SIGNAL(pressed()),  this, SLOT(refresh()));
//    addWidget(refresh);



    for (auto robot = this->reg->getReg().begin(); robot != this->reg->getReg().end(); ++robot ){
        robotSelection->addItem(QString::fromStdString(robot->first));
        map<string, float> conf = robot->second;

        for (auto joint = conf.begin(); joint != conf.end(); ++joint){
            ButtonLayout *buttons = new ButtonLayout(this->app, robot->first, joint->first, joint->second);
            buttons->createWidgets();
            addItem(buttons);
        }

    }



//    addWidget(p);


}

void RobotControlLayout::selectRobot() {

    this->update();
}

void RobotControlLayout::refresh() {
    for(QLayoutItem* item : list_){
        removeItem(item);
        ButtonLayout* button = dynamic_cast<ButtonLayout*>(item);
        if (button){
            delete button;
        }else{
            delete item;
        }
    }
    this->createWidgets();
}