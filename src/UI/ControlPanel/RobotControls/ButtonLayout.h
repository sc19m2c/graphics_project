//
// Created by milo on 16/12/2020.
//

#ifndef COURSEWORK2_BUTTONLAYOUT_H
#define COURSEWORK2_BUTTONLAYOUT_H


#include <QLabel>
#include "../../MasterLayout.h"

class ButtonLayout : public MasterLayout {
    Q_OBJECT
public:
    ButtonLayout(Window *app, string robotName, string name, float angle);

    void setGeometry(const QRect &rect);

    void createWidgets();

    // and a slider for the number of vertices
protected:
    string name;
    float angle;
    QLabel *label;
    QDoubleSpinBox *componentAngle;
    string robotName;
    Register *reg;
    InterfaceController *interface;
public:
    const string &getRobotName() const;

    void setRobotName(const string &robotName);

public slots:
            void setAngle(double d);

};


#endif //COURSEWORK2_BUTTONLAYOUT_H
