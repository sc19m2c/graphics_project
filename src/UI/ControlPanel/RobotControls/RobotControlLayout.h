//
// Created by milo on 16/12/2020.
//

#ifndef COURSEWORK2_ROBOTCONTROLLAYOUT_H
#define COURSEWORK2_ROBOTCONTROLLAYOUT_H


#include "../../MasterLayout.h"

class RobotControlLayout : public MasterLayout {
    Q_OBJECT
public:
    RobotControlLayout(Window *app);

    void setGeometry(const QRect &rect);

    void createWidgets();

    // and a slider for the number of vertices
protected:
    Register *reg;
    QComboBox *robotSelection;

public slots:
    void selectRobot();
    void refresh();

};


#endif //COURSEWORK2_ROBOTCONTROLLAYOUT_H
