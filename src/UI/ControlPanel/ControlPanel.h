//
// Created by milo on 15/12/2020.
//

#ifndef COURSEWORK2_CONTROLPANEL_H
#define COURSEWORK2_CONTROLPANEL_H


#include <QSlider>
#include <QComboBox>
#include <QDoubleSpinBox>
#include "../MasterLayout.h"
#include "RobotControls/RobotControlLayout.h"
#include "AnimationControls/AnimationControlLayout.h"
#include "../ScrollWindow.h"
#include "LightingControl/LightingControl.h"

class ControlPanel : public MasterLayout {
public:
    ControlPanel(Window *app);

    void setGeometry(const QRect &rect);
    void setScroller(ScrollWindow *container, const QRect &r, float y);
    void createWidgets();

    // and a slider for the number of vertices
    QSlider *cameraSlider;

    InterfaceController *interface;
    RobotControlLayout *rcLayout;
    AnimationControlLayout *acLayout;
    LightingControl *lcLayout;
};


#endif //COURSEWORK2_CONTROLPANEL_H
