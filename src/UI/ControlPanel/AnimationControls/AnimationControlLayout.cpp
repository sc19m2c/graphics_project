//
// Created by milo on 18/12/2020.
//

#include <QPushButton>
#include <QSignalMapper>
#include <QLabel>
#include "AnimationControlLayout.h"
AnimationControlLayout::AnimationControlLayout(Window *app): MasterLayout(app){
    this->reg = this->app->getAnInterface()->getReg();
}

void AnimationControlLayout::setGeometry(const QRect &r){
    QLayout::setGeometry(r);

    int c = 1;
    int s = 0;
    int l = 0;
    for (int i = 0; i < list_.size(); i++) {
        QLayoutItem *o = list_.at(i);
        o->setGeometry(QRect(r.x()+10,r.y()+30, (r.width()/2)-20, 30));

        QPushButton* button = dynamic_cast<QPushButton*>(list_.at(i)->widget());
        if (button){

            if(button->objectName().toStdString().compare(this->robotSelection->currentText().toStdString()) == 0){

                button->setGeometry(QRect(r.x()+10,r.y()+(30*(c+1)),(r.width()/2)-20,30));
                c++;
            }
            else{
                button->setGeometry(QRect(0,0,0,0));
            }
            continue;
        }

        QDoubleSpinBox* spin = dynamic_cast<QDoubleSpinBox*>(list_.at(i)->widget());
        if (spin){

            if(spin->objectName().toStdString().compare(this->robotSelection->currentText().toStdString()) == 0){
                if(s%2){
                    spin->setGeometry(QRect(r.x()+10+(r.width()/2)+(r.width()/4),r.y()+(30*(c)),(r.width()/4)-20,30));
                }else{
                    spin->setGeometry(QRect(r.x()+10+(r.width()/2),r.y()+(30*(c)),(r.width()/4)-20,30));
                }
                s++;
            }
            else{
                spin->setGeometry(QRect(0,0,0,0));
            }
            continue;
        }

        QLabel* label = dynamic_cast<QLabel*>(list_.at(i)->widget());
        if (label){

            if(label->objectName().toStdString().compare(this->robotSelection->currentText().toStdString()) == 0){
                if(l%2){
                    label->setGeometry(QRect(r.x()+10+(r.width()/2)+(r.width()/4),r.y()+(30*(c))-30,(r.width()/4)-20,30));

                }else{
                    label->setGeometry(QRect(r.x()+10+(r.width()/2),r.y()+(30*(c))-30,(r.width()/4)-20,30));
                }
                l++;
            }
            else{
                label->setGeometry(QRect(0,0,0,0));
            }
            continue;
        }

    }

}


void AnimationControlLayout::createWidgets(){


    robotSelection = new QComboBox();

    connect(robotSelection, SIGNAL(currentTextChanged(const QString &)),  this, SLOT(selectRobot()));
    addWidget(robotSelection);


    for (auto robot = this->reg->getReg().begin(); robot != this->reg->getReg().end(); ++robot ){
        robotSelection->addItem(QString::fromStdString(robot->first));
        list<string*> paths = reg->getRobotAnimator(robot->first)->getPaths();

        for (string *p : paths){
            QPushButton *button = new QPushButton(QString::fromStdString(*p));
            button->setObjectName(QString::fromStdString(robot->first));
            string robotName = robot->first;
            string filename = *p;
            connect(button, &QPushButton::clicked, [this, robotName,filename] { runAnimation(robotName,filename); });
            addWidget(button);

            QLabel *r_label = new QLabel("radius");
            r_label->setObjectName(QString::fromStdString(robot->first));
            addWidget(r_label);

            QDoubleSpinBox *radius = new QDoubleSpinBox();
            radius->setObjectName(QString::fromStdString(robot->first));
            radius->setRange(0.2, 2);
            radius->setValue(1.);
            radius->setSingleStep(0.2);
            connect(radius, SIGNAL(valueChanged(const double &)),  this, SLOT(changeAnimationRadius(const double &)));
            addWidget(radius);

            QLabel *s_label = new QLabel("speed");
            s_label->setObjectName(QString::fromStdString(robot->first));
            addWidget(s_label);

            QDoubleSpinBox *speed = new QDoubleSpinBox();
            speed->setObjectName(QString::fromStdString(robot->first));
            speed->setRange(0.2, 2);
            speed->setValue(1.);
            speed->setSingleStep(0.2);
            connect(speed, SIGNAL(valueChanged(const double &)),  this, SLOT(changeAnimationSpeed(const double &)));
            addWidget(speed);

        }
    }

}


void AnimationControlLayout::selectRobot() {
    this->update();
}

void AnimationControlLayout::runAnimation(string robotName, string name){
    int currentTime = this->app->getAnInterface()->getCurrentTime();
    this->reg->getRobotAnimator(robotName)->loadKeyFrameSequence(name, currentTime);
}

void AnimationControlLayout::changeAnimationRadius(double d) {
    this->reg->getRobotAnimator(this->robotSelection->currentText().toStdString())->setRadius(d);
}

void AnimationControlLayout::changeAnimationSpeed(double d) {
    this->reg->getRobotAnimator(this->robotSelection->currentText().toStdString())->setSpeed(d);
}