//
// Created by milo on 18/12/2020.
//

#ifndef COURSEWORK2_ANIMATIONCONTROLLAYOUT_H
#define COURSEWORK2_ANIMATIONCONTROLLAYOUT_H


#include "../../MasterLayout.h"

class AnimationControlLayout : public MasterLayout {
    Q_OBJECT
public:
    AnimationControlLayout(Window *app);

    void setGeometry(const QRect &rect);

    void createWidgets();

    // and a slider for the number of vertices
protected:
    Register *reg;
    QComboBox *robotSelection;

public slots:
    void selectRobot();
    void runAnimation(string robotName, string name);
    void changeAnimationSpeed(double);
    void changeAnimationRadius(double);

};


#endif //COURSEWORK2_ANIMATIONCONTROLLAYOUT_H
