//
// Created by milo on 15/12/2020.
//

#ifndef COURSEWORK2_MASTERLAYOUT_H
#define COURSEWORK2_MASTERLAYOUT_H


#include <QLayout>
#include <QWidget>
#include "../Window.h"


class MasterLayout : public QLayout {
    Q_OBJECT
public:
    MasterLayout(Window *app);
    ~MasterLayout();

    // standard functions for a QLayout
    virtual void setGeometry(const QRect &rect);
    virtual void createWidgets();

    void addItem(QLayoutItem *item);
    QSize sizeHint() const;
    QSize minimumSize() const;
    int count() const;
    QLayoutItem *itemAt(int) const;
    QLayoutItem *takeAt(int);

protected:
    QList<QLayoutItem*> list_;
    Window *app;
    //DcomponentNav *getNavbar();


    //DcomponentNav *navBar;


};


#endif //COURSEWORK2_MASTERLAYOUT_H
