//
// Created by milo on 15/12/2020.
//

#include "MasterLayout.h"

MasterLayout::MasterLayout(Window *app): QLayout() {
    this->app = app;
}

// following methods provide a trivial list-based implementation of the QLayout class
int MasterLayout::count() const {
    return list_.size();
}

QLayoutItem *MasterLayout::itemAt(int idx) const {
    return list_.value(idx);
}

QLayoutItem *MasterLayout::takeAt(int idx) {
    return idx >= 0 && idx < list_.size() ? list_.takeAt(idx) : 0;
}

void MasterLayout::addItem(QLayoutItem *item) {
    list_.append(item);
}


QSize MasterLayout::sizeHint() const {
    return minimumSize();
}

QSize MasterLayout::minimumSize() const {
    return QSize(320,320);
}

MasterLayout::~MasterLayout() {
    QLayoutItem *item;
    while ((item = takeAt(0)))
        delete item;
}

void MasterLayout::setGeometry(const QRect &rect){

};
void MasterLayout::createWidgets(){

}

