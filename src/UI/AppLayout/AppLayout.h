//
// Created by milo on 15/12/2020.
//

#ifndef COURSEWORK2_APPLAYOUT_H
#define COURSEWORK2_APPLAYOUT_H


#include "../MasterLayout.h"


class AppLayout : public MasterLayout {
public:
    AppLayout(Window *app) : MasterLayout(app) {};

    void setGeometry(const QRect &rect);

    void createWidgets();



};


#endif //COURSEWORK2_APPLAYOUT_H
