//
// Created by milo on 15/12/2020.
//

#include <QPushButton>
#include <iostream>
#include "AppLayout.h"
#include "../ControlPanel/ControlPanel.h"
#include "../GraphicsPanel/GraphicsPanel.h"

using namespace std;
void AppLayout::setGeometry(const QRect &r){

    QLayout::setGeometry(r);

    for (int i = 0; i < list_.size(); i++) {

        QLayoutItem *o = list_.at(i);

        QPushButton* backS = dynamic_cast<QPushButton*>(list_.at(i)->widget());
        if (backS){
            backS->setGeometry(QRect(r.x()+10,r.y()+30,70,30));
            continue;
        }

        MasterLayout* controlPanel = dynamic_cast<ControlPanel*>(list_.at(i));
        if (controlPanel){
            controlPanel->setGeometry(QRect(r.width()/2,r.y(),r.width()/2,r.height()));
            continue;
        }

        MasterLayout* graphicsPanel = dynamic_cast<GraphicsPanel*>(list_.at(i));
        if (graphicsPanel){
            graphicsPanel->setGeometry(QRect(r.x(),r.y(),r.width()/2, r.height()));
            continue;
        }

    }
}

void AppLayout::createWidgets() {
//    QPushButton *p = new QPushButton("push");
//    p->setStyleSheet("color: blue; background-color: yellow");
//
//    addWidget(p);


    GraphicsPanel *graphicsPanel = new GraphicsPanel(this->app);
    graphicsPanel->createWidgets();
    this->app->setAnInterface(graphicsPanel->interface);
    addItem(graphicsPanel);

    ControlPanel *controlPanel = new ControlPanel(this->app);
    controlPanel->createWidgets();
    addItem(controlPanel);
}