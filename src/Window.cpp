//
// Created by milo on 30/11/2020.
//

#include <iostream>
#include "Window.h"
#include "UI/AppLayout/AppLayout.h"

using namespace std;

// constructor / destructor
Window::Window(QWidget *parent)
        : QWidget(parent)
{ // constructor

    // create menu bar
    menuBar = new QMenuBar(this);

    // create file menu
    fileMenu = menuBar->addMenu("&File");

    // create the action
    actionQuit = new QAction("&Quit", this);

    // leave signals & slots to the controller

    // add the item to the menu
    fileMenu->addAction(actionQuit);

    cout << this << endl;

    AppLayout *temp = new AppLayout(this);
    temp->createWidgets();
    setLayout(temp);


    ptimer = new QTimer(this);
    ptimer->start(200);
    connect(ptimer, SIGNAL(timeout()),  interface, SLOT(update_positions()));

} // constructor

Window::~Window()
{ // destructor
    delete cameraSlider;
    delete interface;
    delete windowLayout;
    delete actionQuit;
    delete fileMenu;
    delete menuBar;

} // destructor

// resets all the interface elements
void Window::ResetInterface()
{ // ResetInterface()
    cameraSlider->setMinimum(0);
    cameraSlider->setMaximum(300);
    cameraSlider->setValue(0);
    //don't use the slider for now

    //	nVerticesSlider->setValue(thePolygon->nVertices);

    // now force refresh
    interface->update();
    update();
}

InterfaceController *Window::getAnInterface() const {
    return interface;
}

void Window::setAnInterface(InterfaceController *anInterface) {
    interface = anInterface;
}
// ResetInterface()