//
// Created by milo on 22/12/2020.
//

#include <iostream>
#include "Textures.h"
#include "Image.h"

using namespace std;

Textures::Textures() {}

GLuint Textures::flatMaterialTextures[16];
GLuint Textures::repeatMaterialTextures[16];
GLuint Textures::backgroundTextures[16];
bool Textures::isTextures = true;

void Textures::loadFlatTextures(std::vector<std::string> faces){

    glGenTextures(16, flatMaterialTextures);

    int width, height, nrChannels;
    for (unsigned int i = 0; i < faces.size(); i++) {
        glBindTexture(GL_TEXTURE_2D, flatMaterialTextures[i]);

        Image im(faces[i]);

        glTexImage2D(GL_TEXTURE_2D,
                     0, GL_RGB, im.Width(), im.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, im.imageField()
        );

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);



    }
}

void Textures::loadRepeatTextures(std::vector<std::string> faces){

    glGenTextures(16, repeatMaterialTextures);

    int width, height, nrChannels;
    for (unsigned int i = 0; i < faces.size(); i++) {
        glBindTexture(GL_TEXTURE_2D, repeatMaterialTextures[i]);

        Image im(faces[i]);

        glTexImage2D(GL_TEXTURE_2D,
                     0, GL_RGB, im.Width(), im.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, im.imageField()
        );

        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);



    }
}

void Textures::loadBackgroundTextures(std::vector<std::string> faces){

    glGenTextures(16, backgroundTextures);

    int width, height, nrChannels;
    for (unsigned int i = 0; i < faces.size(); i++) {
        glBindTexture(GL_TEXTURE_2D, backgroundTextures[i]);

        Image im(faces[i]);

        glTexImage2D(GL_TEXTURE_2D,
                     0, GL_RGB, im.Width(), im.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, im.imageField()
        );



        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);



    }
}

bool Textures::isTextures1() {
    return isTextures;
}

void Textures::setIsTextures(bool isTextures) {
    Textures::isTextures = isTextures;
}

