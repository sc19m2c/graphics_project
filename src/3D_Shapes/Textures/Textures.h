//
// Created by milo on 22/12/2020.
//

#ifndef COURSEWORK2_TEXTURES_H
#define COURSEWORK2_TEXTURES_H


#include <QtGui>

class Textures {

public:
    Textures();
    void loadFlatTextures(std::vector<std::string> faces);
    void loadRepeatTextures(std::vector<std::string> faces);
    void loadBackgroundTextures(std::vector<std::string> faces);

    static GLuint flatMaterialTextures[];
    static GLuint repeatMaterialTextures[];
    static GLuint backgroundTextures[];
protected:
    static bool isTextures;
public:
    static bool isTextures1();

    static void setIsTextures(bool isTextures);

};

#endif //COURSEWORK2_TEXTURES_H
