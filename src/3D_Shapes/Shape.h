//
// Created by milo on 12/12/2020.
//

#ifndef COURSEWORK2_SHAPE_H
#define COURSEWORK2_SHAPE_H

#include "../Materials/Material.h"
#include "Joint.h"
#include "../Types/Offset.h"


#include <QGLWidget>

struct RepeatParam{
    float rx;
    float ry;
};

class Shape : public QGLWidget , public Joint{ //
    Q_OBJECT

public:
    Shape(QWidget *parent);
    Shape(QWidget *parent, float x_width, float y_height, float z_depth);
    ~Shape();
    virtual void initTranformation();
    virtual void draw() = 0;

    materialStruct* get_material();
    float get_x_width();
    float get_y_depth();
    float get_z_height();

    void set_material(materialStruct* material);
    void set_x_width(float x_width);
    void set_y_depth(float y_height);
    void set_z_height(float z_depth);
    void set_isShadow(bool isShadow);

    Shape *getParentShape();
    void setParentShape(Shape *parentShape);

    Offset *getOffset() const;
    void setOffset(Offset *offset);

    void enableTexture();


protected:
    bool isShadow = false;
public:
    static const QMetaObject &getStaticMetaObject();

protected:
    float x_width = 2;
    float z_depth = 2;
    float y_height = 2;

    materialStruct* material;
    GLuint textureId = -1;

    RepeatParam rp = {1.0,1.0};

    Offset *offset;
public:
    const RepeatParam &getRp() const;

    void setRp(const RepeatParam &rp);

public:
    GLuint getTextureId() const;

    void setTextureId(GLuint textureId);

protected:
    Shape *parentShape = nullptr;

};


#endif //COURSEWORK2_SHAPE_H
