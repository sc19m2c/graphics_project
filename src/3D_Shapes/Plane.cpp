//
// Created by milo on 13/12/2020.
//

#include "Plane.h"
#include "Textures/Textures.h"
#include<iostream>

using namespace std;

// constructor
Plane::Plane(QWidget *parent)
        : Shape(parent){ // constructor


} // constructor

// constructor
Plane::Plane(QWidget *parent, float x_width, float y_depth, float z_height)
        : Shape(parent, x_width, y_depth, z_height){ // constructor

} // constructor

void Plane::draw(){

    glMaterialfv(GL_FRONT, GL_AMBIENT,    this->material->ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,    this->material->diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,   this->material->specular);
    glMaterialf(GL_FRONT, GL_SHININESS,   this->material->shininess);

    if (this->isShadow == true){
        glDisable(GL_LIGHTING);
        glDisable(GL_TEXTURE_2D);
        glColor3f(0.,0.,0.);
    }else{
        if (this->textureId == -1 || !Textures::isTextures1()){
            glDisable(GL_TEXTURE_2D);
        }
        else{
            this->enableTexture();
        }
    }



    glNormal3f(0.,1.,0.);
    glBegin(GL_POLYGON);
        glTexCoord2f(rp.rx, 0.0);    glVertex3f( -1.0,  0,  1);
        glTexCoord2f(0.0, 0.0);    glVertex3f(  1.0,  0,  1);
        glTexCoord2f(0.0, rp.ry);     glVertex3f(  1.0,  0, -1);
        glTexCoord2f(rp.rx, rp.ry);    glVertex3f( -1.0,  0, -1);
    glEnd();

    glEnable(GL_TEXTURE_2D);
}