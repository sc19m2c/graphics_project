//
// Created by milo on 12/12/2020.
//

#include "Shape.h"
#include "../Materials/Material.cpp"
#include "../Types/Offset.cpp"
#include <iostream>
using namespace std;

Shape:: ~Shape(){
    delete parentShape;
}


Shape::Shape(QWidget *parent)
        : QGLWidget(parent), Joint(){ // constructor
    this->material = &blueShinyMaterials;
    this->offset = &defaultOffset;
    if(this->parentShape)this->isShadow = this->parentShape->isShadow;

} // constructor

Shape::Shape(QWidget *parent, float x_width, float y_height, float z_depth )
        : QGLWidget(parent), Joint(){ // constructor
    this->x_width = x_width;
    this->y_height = y_height;
    this->z_depth = z_depth;
    this->material = &blueShinyMaterials;
    this->offset = &defaultOffset;
    if(this->parentShape)this->isShadow = this->parentShape->isShadow;
} // constructor

void Shape::initTranformation(){

    Shape *p = this->parentShape;

    if(p){
        glTranslatef(0,p->y_height,0);

        glTranslatef(this->offset->offset[0],this->offset->offset[1],this->offset->offset[2]);
    }

    switch(this->axis)
    {
        case Xa  : glRotatef(this->angle, 1,0,0); break;
        case Ya: glRotatef(this->angle, 0,1,0); break;
        case Za : glRotatef(this->angle, 0,0,1); break;
    }
}

void Shape::enableTexture(){

    materialStruct *p_front = &whiteShinyMaterials;

    glMaterialfv(GL_FRONT, GL_AMBIENT, p_front->ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, p_front->diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, p_front->specular);
    glMaterialf(GL_FRONT, GL_SHININESS, p_front->shininess);

    glBindTexture(GL_TEXTURE_2D, textureId);
}


materialStruct* Shape::get_material(){
    return this->material;
}
float Shape::get_x_width(){
    return this->x_width;
}
float Shape::get_y_depth(){
    return this->y_height;
}
float Shape::get_z_height(){
    return this->z_depth;
}

void Shape::set_material(materialStruct *material) {
    this->material = material;
}

void Shape::set_x_width(float x_width){
    this->x_width = x_width;
}
void Shape::set_y_depth(float y_height){
    this->y_height = y_height;
}
void Shape::set_z_height(float z_depth){
    this->z_depth = z_depth;
}
void Shape::set_isShadow(bool isShadow){
    this->isShadow = isShadow;
}

void Shape::setParentShape(Shape *parentShape) {
    Shape::parentShape = parentShape;
}

Shape *Shape::getParentShape() {
    return parentShape;
}

Offset *Shape::getOffset() const {
    return offset;
}

void Shape::setOffset(Offset *offset) {
    Shape::offset = offset;
}

const QMetaObject &Shape::getStaticMetaObject() {
    return staticMetaObject;
}

GLuint Shape::getTextureId() const {
    return textureId;
}

void Shape::setTextureId(GLuint textureId) {
    Shape::textureId = textureId;
}

const RepeatParam &Shape::getRp() const {
    return rp;
}

void Shape::setRp(const RepeatParam &rp) {
    Shape::rp = rp;
}




