//
// Created by milo on 17/01/2021.
//

#ifndef COURSEWORK2_CONVEXSHAPE_H
#define COURSEWORK2_CONVEXSHAPE_H


#include "Shape.h"
#include<vector>
#include<cmath>
using namespace std;

class ConvexShape : public Shape{ //

    Q_OBJECT

public:
    ConvexShape(QWidget *parent);
    ConvexShape(QWidget *parent, float x_width, float y_depth, float z_height);
    float* subVector(vector<float>  v1, vector<float>  v2);
    float* calcNormal(vector<float> p1, vector<float> p2, vector<float> p3);
    void draw();
};


#endif //COURSEWORK2_CONVEXSHAPE_H
