//
// Created by milo on 13/12/2020.
//

#include "Joint.h"



Joint::Joint(){

}



void Joint::setAngle(float angle) {
    Joint::angle = angle;
}

float Joint::getAngle() const {
    return angle;
}

Axis Joint::getAxis() const {
    return axis;
}

void Joint::setAxis(Axis axis) {
    Joint::axis = axis;
}
