//
// Created by milo on 12/12/2020.
//

#ifndef COURSEWORK2_SPHERE_H
#define COURSEWORK2_SPHERE_H


#include <QWidget>
#include <QGLWidget>
#include "Shape.h"

class Sphere  : public Shape{ //

Q_OBJECT

public:
    Sphere(QWidget *parent);
    Sphere(QWidget *parent, float x_width, float y_depth, float z_height);
    void draw();

    void setSpotRotation(void (*spotRotation)());

    void (*spotRotation)() = {};


};


#endif //COURSEWORK2_SPHERE_H
