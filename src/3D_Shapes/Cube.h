//
// Created by milo on 01/12/2020.
//

#ifndef COURSEWORK2_CUBE_H
#define COURSEWORK2_CUBE_H


#include <QGLWidget>
#include <QObject>
#include "Shape.h"
#include "Joint.h"

class Cube : public Shape{ //

Q_OBJECT

public:
    Cube(QWidget *parent);
    Cube(QWidget *parent, float x_width, float y_depth, float z_height);
    void draw();
};


#endif //COURSEWORK2_CUBE_H
