//
// Created by milo on 17/01/2021.
//

#include "ConvexShape.h"
#include<iostream>
// constructor
ConvexShape::ConvexShape(QWidget *parent)
        : Shape(parent){ // constructor

} // constructor

// constructor
ConvexShape::ConvexShape(QWidget *parent, float x_width, float y_height, float z_depth)
        : Shape(parent, x_width, y_height, z_depth){ // constructor

} // constructor

float *ConvexShape::subVector(vector<float>  v1, vector<float>  v2){

    float *sv = new float[3];
    sv[0] = v1[0] - v2[0];
    sv[1] = v1[1]-v2[1];
    sv[2] = v1[2]-v2[2];
    return sv;

}


float* ConvexShape::calcNormal(vector<float> p1, vector<float> p2, vector<float> p3){

    float *A = this->subVector(p2, p1);
    float *B = this->subVector(p3, p1);

    float N[3] = {0};
    N[0] = A[1]*B[2] - A[2]*B[1];
    N[1] = A[2]*B[0] - A[0]*B[2];
    N[2] = A[0]*B[1] - A[1]*B[0];

    float mag = sqrt((pow(N[0],2)+pow(N[1],2)+pow(N[2],2)));

    float *norm = new float[3];
    norm[0] = -(N[0]/mag);
    norm[1] = -(N[1]/mag);
    norm[2] = -(N[2]/mag);

    return norm;

}



void ConvexShape::draw() {


    glPushMatrix();

    glTranslatef(0,this->y_height/2,0);
    glScalef(this->x_width/2, this->y_height/2, this->z_depth/2);


    glMaterialfv(GL_FRONT, GL_AMBIENT, this->material->ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, this->material->diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, this->material->specular);
    glMaterialf(GL_FRONT, GL_SHININESS, this->material->shininess);



    if (this->isShadow == true){
        glDisable(GL_LIGHTING);
        glDisable(GL_TEXTURE_2D);
        glColor3f(0.,0.,0.);
    }else{
//        if (this->textureId == -1 || !Textures::isTextures1()){
//            glDisable(GL_TEXTURE_2D);
//        }
//        else{
//            this->enableTexture();
//        }
    }



//    vector<vector<float>> points = {
//            {0.0,0.36,0.09},{0.0, 0.81, 0.36},{0.81 ,0.45 ,0.36},{0.36 ,0.45 ,0.72},{0.36 ,0.72 ,0.09},
//            {0.72 ,0.9 ,0.09},{0.72 ,0.27 ,0.9},{0.45 ,0.27 ,0.72},{0.45, 0.18 ,0.27},{0.18 ,0.63 ,0.27},
//            {0.63 ,0.9 ,0.27},{0.63, 0.54 ,0.9},{0.63 ,1.0 ,0.54},{1.0 ,0.0 ,0.54},{0.0 ,0.09 ,0.54},
//            {0.54 ,0.09 ,0.9},{0.81 ,0.0 ,1.0},{0.81 ,1.0 ,0.18},{0.81 ,0.18 ,0.45},{0.63 ,0.18 ,1.0}
//    };
//
//
//
//    glBegin(GL_TRIANGLES);
//    for (int i = 0 ; i < points.size()-2; i++){
//
//
//        vector<vector<float>> triangle = {points.begin() + i, points.begin()+i+3};
//        cout << triangle.size() << endl;
//        float *normal = this->calcNormal(triangle[0], triangle[1], triangle[2]);
//
//        cout << "after normal" << endl;
//        cout << normal[0] << " " << normal[1] << " " << normal[2] << endl;
//
//        glNormal3fv(normal);
////        glTexCoord2f(d_phi, 0);
////        glTexCoord2f(static_cast<float>(longit+1)/nr_phi, static_cast<float>(lat)/nr_theta);
//        glVertex3f(triangle[0][0],triangle[0][1],triangle[0][2]);
//        glVertex3f(triangle[1][0],triangle[1][1],triangle[1][2]);
//        glVertex3f(triangle[2][0],triangle[2][1],triangle[2][2]);
//
//
//    }
//    glEnd();

    float X= .525731112119133606;
    float Z= .850650808352039932;

    const vector<vector<float>> vdata =
            {
                    {-X,0.0,Z}, {X,0.0,Z}, {-X,0.0,-Z}, {X,0.0,-Z},
                    {0.0,Z,X}, {0.0,Z,-X}, {0.0,-Z,X}, {0.0,-Z,-X},
                    {Z,X,0.0}, {-Z,X,0.0}, {Z,-X,0.0}, {-Z,-X,0.0},
            };

    const GLuint tindices[20][3] =
            {
                    {0,4,1}, {0,9,4}, {9,5,4}, {4,5,8}, {4,8,1},
                    {8,10,1}, {8,3,10}, {5,3,8}, {5,2,3}, {2,7,3},
                    {7,10,3}, {7,6,10}, {7,11,6}, {11,0,6}, {0,1,6},
                    {6,1,10}, {9,0,11}, {9,11,2}, {9,2,5}, {7,2,11}
            };

    int i;
    GLfloat nx,ny,nz;

//    glEnable(GL_CULL_FACE);
//    glFrontFace(GL_CW);
    glBegin(GL_TRIANGLES);
    for (i = 0; i < 20; i++)
    {


        float *normal = this->calcNormal(vdata[tindices[i][0]], vdata[tindices[i][1]], vdata[tindices[i][2]]);

        glNormal3fv(normal);

        glVertex3f(vdata[tindices[i][0]][0],vdata[tindices[i][0]][1],vdata[tindices[i][0]][2]);
        glVertex3f(vdata[tindices[i][1]][0],vdata[tindices[i][1]][1],vdata[tindices[i][1]][2]);
        glVertex3f(vdata[tindices[i][2]][0],vdata[tindices[i][2]][1],vdata[tindices[i][2]][2]);

    }
    glEnd();


    glEnable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);
    glDisable( GL_BLEND );

    glPopMatrix();

}