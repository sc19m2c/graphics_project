//
// Created by milo on 12/12/2020.
//

#include "Sphere.h"
#include "Textures/Textures.h"
#include <QGLWidget>
#include <cmath>
#include <iostream>


// constructor
Sphere::Sphere(QWidget *parent)
        : Shape(parent){ // constructor


} // constructor

// constructor
Sphere::Sphere(QWidget *parent, float x_width, float y_height, float z_depth)
        : Shape(parent, x_width, y_height, z_depth){ // constructor

} // constructor

void Sphere::draw(){
    glPushMatrix();

    glTranslatef(0,this->y_height/2,0);
    glScalef(this->x_width/2, this->y_height/2, this->z_depth/2);

    spotRotation();


    if (this->isShadow == true){
        glDisable(GL_LIGHTING);
        glColor3f(0.,0.,0.);
    }else{
        if (this->textureId == -1 || !Textures::isTextures1()){
            glDisable(GL_TEXTURE_2D);
        }
        else{
            this->enableTexture();
        }
    }




    int nr_phi = 20;
    int nr_theta = 20;
    float pi = 3.14159265358979323846;

    for(int longit = 0; longit < nr_phi; longit++){
        for(int lat = 0; lat < nr_theta; lat++){
            float d_phi = 2*pi/nr_phi;
            float d_theta = pi/nr_theta;

            glBegin(GL_TRIANGLES);
            double x,y,z;

            x = cos(longit*d_phi)*sin(lat*d_theta);
            y = sin(longit*d_phi)*sin(lat*d_theta);
            z = cos(lat*d_theta);
            glNormal3f(x,y,z);
            glTexCoord2f(static_cast<float>(longit)/nr_phi, static_cast<float>(lat)/nr_theta);
            glVertex3f(x,y,z);

            x = cos((longit+1)*d_phi)*sin(lat*d_theta);
            y = sin((longit+1)*d_phi)*sin(lat*d_theta);
            z = cos(lat*d_theta);
            glNormal3f(x,y,z);
            glTexCoord2f(d_phi, 0);
            glTexCoord2f(static_cast<float>(longit+1)/nr_phi, static_cast<float>(lat)/nr_theta);
            glVertex3f(x,y,z);

            x = cos((longit+1)*d_phi)*sin((lat+1)*d_theta);
            y = sin((longit+1)*d_phi)*sin((lat+1)*d_theta);
            z = cos((lat+1)*d_theta);
            glNormal3f(x,y,z);
            glTexCoord2f(static_cast<float>(longit+1)/nr_phi, static_cast<float>(lat+1)/nr_theta);
            glVertex3f(x,y,z);



            ///


            x = cos(longit*d_phi)*sin(lat*d_theta);
            y = sin(longit*d_phi)*sin(lat*d_theta);
            z = cos(lat*d_theta);
            glNormal3f(x,y,z);
            glTexCoord2f(static_cast<float>(longit)/nr_phi, static_cast<float>(lat)/nr_theta);
            glVertex3f(x,y,z);

            x = cos((longit+1)*d_phi)*sin((lat+1)*d_theta);
            y = sin((longit+1)*d_phi)*sin((lat+1)*d_theta);
            z = cos((lat+1)*d_theta);
            glNormal3f(x,y,z);
            glTexCoord2f(static_cast<float>(longit+1)/nr_phi, static_cast<float>(lat+1)/nr_theta);
            glVertex3f(x,y,z);

            x = cos((longit)*d_phi)*sin((lat+1)*d_theta);
            y = sin((longit)*d_phi)*sin((lat+1)*d_theta);
            z = cos((lat+1)*d_theta);
            glNormal3f(x,y,z);
            glTexCoord2f(d_phi, 0);
            glTexCoord2f(static_cast<float>(longit)/nr_phi, static_cast<float>(lat+1)/nr_theta);
            glVertex3f(x,y,z);


            glEnd();

        }
    }

    glEnable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);
    glPopMatrix();
}

void Sphere::setSpotRotation(void (*spotRotation)()) {
    Sphere::spotRotation = spotRotation;
}
