//
// Created by milo on 12/12/2020.
//

#ifndef COURSEWORK2_CYLINDER_H
#define COURSEWORK2_CYLINDER_H


#include <QWidget>
#include <QGLWidget>
#include "Shape.h"
#include "Joint.h"

enum Orientation { X = 0, Y = 1, Z= 2 };

class Cylinder : public Shape{ //

Q_OBJECT

public:
    Cylinder(QWidget *parent);
    Cylinder(QWidget *parent, float x_width, float y_depth, float z_height);
    void draw();
    Orientation orientation = X;

    void setOrientation(Orientation orientation);

};


#endif //COURSEWORK2_CYLINDER_H
