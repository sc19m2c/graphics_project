//
// Created by milo on 30/11/2020.
//

#ifndef COURSEWORK2_PYRAMID_H
#define COURSEWORK2_PYRAMID_H

#include <QGLWidget>
#include <QObject>
#include "Shape.h"


class Pyramid : public Shape{  //

Q_OBJECT

public:
    Pyramid(QWidget *parent);
    Pyramid(QWidget *parent, float x_width, float y_depth, float z_height);
    void draw();
protected:
    void (*spotRotation)() = {};

public:


    void setSpotRotation(void (*spotRotation)());


}; // class GLPolygonWidget


#endif //COURSEWORK2_PYRAMID_H
