//
// Created by milo on 01/12/2020.
//

#include "Cube.h"
#include "Textures/Textures.h"
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <iostream>
using namespace std;

// constructor
Cube::Cube(QWidget *parent)
        : Shape(parent){ // constructor

} // constructor

// constructor
Cube::Cube(QWidget *parent, float x_width, float y_height, float z_depth)
        : Shape(parent, x_width, y_height, z_depth){ // constructor

} // constructor

void Cube::draw() {


    glPushMatrix();

    glTranslatef(0,this->y_height/2,0);
    glScalef(this->x_width/2, this->y_height/2, this->z_depth/2);

    GLfloat normals[][3] = { {1., 0. ,0.}, {-1., 0., 0.}, {0., 0., 1.}, {0., 0., -1.}, {0, 1, 0}, {0, -1, 0} };

    glMaterialfv(GL_FRONT, GL_AMBIENT, this->material->ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, this->material->diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, this->material->specular);
    glMaterialf(GL_FRONT, GL_SHININESS, this->material->shininess);



    if (this->isShadow == true){
        glDisable(GL_LIGHTING);
        glDisable(GL_TEXTURE_2D);
        glColor3f(0.,0.,0.);
    }else{
        if (this->textureId == -1 || !Textures::isTextures1()){
            glDisable(GL_TEXTURE_2D);
        }
        else{
            this->enableTexture();
        }
    }


    //right facce
    glNormal3fv(normals[0]);
    glBegin(GL_POLYGON);
    glTexCoord2f(rp.rx, 0.0);    glVertex3f( 1.0, -1.0,  1.0);
    glTexCoord2f(0.0, 0.0);    glVertex3f( 1.0, -1.0, -1.0);
    glTexCoord2f(0.0, rp.ry);    glVertex3f( 1.0,  1.0, -1.0);
    glTexCoord2f(rp.rx, rp.ry);    glVertex3f( 1.0,  1.0,  1.0);
    glEnd();


    glNormal3fv(normals[3]);
    glBegin(GL_POLYGON);
    glTexCoord2f(rp.rx, 0.0);    glVertex3f(-1.0, -1.0, -1.0);
    glTexCoord2f(0.0, 0.0);    glVertex3f( 1.0, -1.0, -1.0);
    glTexCoord2f(0.0, rp.ry);    glVertex3f( 1.0,  1.0, -1.0);
    glTexCoord2f(rp.rx, rp.ry);    glVertex3f(-1.0,  1.0, -1.0);
    glEnd();


    glNormal3fv(normals[2]);
    glBegin(GL_POLYGON);
    glTexCoord2f(rp.rx, 0.0);    glVertex3f(-1.0, -1.0, 1.0);
    glTexCoord2f(0.0, 0.0);    glVertex3f( 1.0, -1.0, 1.0);
    glTexCoord2f(0.0, rp.ry);    glVertex3f( 1.0,  1.0, 1.0);
    glTexCoord2f(rp.rx, rp.ry);    glVertex3f(-1.0,  1.0, 1.0);
    glEnd();

    glNormal3fv(normals[1]);
    glBegin(GL_POLYGON);
    glTexCoord2f(rp.rx, 0.0);    glVertex3f( -1.0, -1.0,  1.0);
    glTexCoord2f(0.0, 0.0);    glVertex3f( -1.0, -1.0, -1.0);
    glTexCoord2f(0.0, rp.ry);    glVertex3f( -1.0,  1.0, -1.0);
    glTexCoord2f(rp.rx, rp.ry);    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();


    glNormal3fv(normals[4]);
    glBegin(GL_POLYGON);
    glTexCoord2f(rp.rx, 0.0);    glVertex3f(  1.0,  1.0,  1.0);
    glTexCoord2f(0.0, 0.0);    glVertex3f(  1.0,  1.0, -1.0);
    glTexCoord2f(0.0, rp.ry);    glVertex3f( -1.0,  1.0, -1.0);
    glTexCoord2f(rp.rx, rp.ry);    glVertex3f( -1.0,  1.0,  1.0);
    glEnd();


    glNormal3fv(normals[5]);
    glBegin(GL_POLYGON);
    glTexCoord2f(rp.rx, 0.0);    glVertex3f(  1.0,  -1.0,  1.0);
    glTexCoord2f(0.0, 0.0);    glVertex3f(  1.0,  -1.0, -1.0);
    glTexCoord2f(0.0, rp.ry);    glVertex3f( -1.0,  -1.0, -1.0);
    glTexCoord2f(rp.rx, rp.ry);    glVertex3f( -1.0,  -1.0,  1.0);
    glEnd();


    glEnable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);
    glDisable( GL_BLEND );

    glPopMatrix();

}

//void Cube::draw() {
//
//    if (this->isShadow == true){
//        std::cout << "print this line setting to black" << std::endl;
//        glDisable(GL_LIGHTING);
//        glColor3f(0.,0.,0.);
//    }
//
//
//    glMaterialfv(GL_FRONT, GL_AMBIENT, this->material->ambient);
//    glMaterialfv(GL_FRONT, GL_DIFFUSE, this->material->diffuse);
//    glMaterialfv(GL_FRONT, GL_SPECULAR, this->material->specular);
//    glMaterialf(GL_FRONT, GL_SHININESS, this->material->shininess);
//
//    glColor3f(0.0, 0.0, 1.0);   // blue; facing positive x-axis
//    //bottom
//    float length = 1.0;
//    glm::vec3 v1 = {length, -length, -length};
//    glm::vec3 v2 = {length, length, -length};
//    glm::vec3 v3 = {-length, length, -length};
//    glm::vec3 v4 = {-length, -length, -length};
//
//    glm::vec3 n = glm::normalize(glm::cross(v2 - v1, v3 - v2));
//
//    glNormal3fv(glm::value_ptr(n));
//    glBegin(GL_POLYGON);
//    glVertex3f(v1[0], v1[1], v1[2]);
//    glVertex3f(v2[0], v2[1], v2[2]);
//    glVertex3f(v3[0], v3[1], v3[2]);
//    glVertex3f(v4[0], v4[1], v4[2]);
//    glEnd();
//
//    //top
//    v1 = {length, -length, length};
//    v2 = {length, length, length};
//    v3 = {-length, length, length};
//    v4 = {-length, -length, length};
//
//    n = glm::normalize(glm::cross(v2 - v1, v3 - v2));
//
//    glNormal3fv(glm::value_ptr(n));
//    glBegin(GL_POLYGON);
//    glVertex3f(v1[0], v1[1], v1[2]);
//    glVertex3f(v2[0], v2[1], v2[2]);
//    glVertex3f(v3[0], v3[1], v3[2]);
//    glVertex3f(v4[0], v4[1], v4[2]);
//    glEnd();
//
//    //front
//    v1 = {length, -length, -length};
//    v2 = {length, -length, length};
//    v3 = {-length, -length, length};
//    v4 = {-length, -length, -length};
//
//    n = glm::normalize(glm::cross(v2 - v1, v3 - v2));
//
//    glNormal3fv(glm::value_ptr(n));
//    glBegin(GL_POLYGON);
//    glVertex3f(v1[0], v1[1], v1[2]);
//    glVertex3f(v2[0], v2[1], v2[2]);
//    glVertex3f(v3[0], v3[1], v3[2]);
//    glVertex3f(v4[0], v4[1], v4[2]);
//    glEnd();
//
//    //back
//    v1 = {length, length, -length};
//    v2 = {length, length, length};
//    v3 = {-length, length, length};
//    v4 = {-length, length, -length};
//
//    n = glm::normalize(glm::cross(v2 - v1, v3 - v2));
//
//    glNormal3fv(glm::value_ptr(n));
//    glBegin(GL_POLYGON);
//    glVertex3f(v1[0], v1[1], v1[2]);
//    glVertex3f(v2[0], v2[1], v2[2]);
//    glVertex3f(v3[0], v3[1], v3[2]);
//    glVertex3f(v4[0], v4[1], v4[2]);
//    glEnd();
//
//    //side-right
//    v1 = {length, length, -length};
//    v2 = {length, length, length};
//    v3 = {length, -length, length};
//    v4 = {length, -length, -length};
//
//    n = glm::normalize(glm::cross(v2 - v1, v3 - v2));
//
//    glNormal3fv(glm::value_ptr(n));
//    glBegin(GL_POLYGON);
//    glVertex3f(v1[0], v1[1], v1[2]);
//    glVertex3f(v2[0], v2[1], v2[2]);
//    glVertex3f(v3[0], v3[1], v3[2]);
//    glVertex3f(v4[0], v4[1], v4[2]);
//    glEnd();
//
//    //side-left
//    v1 = {-length, length, -length};
//    v2 = {-length, length, length};
//    v3 = {-length, -length, length};
//    v4 = {-length, -length, -length};
//
//    n = glm::normalize(glm::cross(v2 - v1, v3 - v2));
//
//    glNormal3fv(glm::value_ptr(n));
//    glBegin(GL_POLYGON);
//    glVertex3f(v1[0], v1[1], v1[2]);
//    glVertex3f(v2[0], v2[1], v2[2]);
//    glVertex3f(v3[0], v3[1], v3[2]);
//    glVertex3f(v4[0], v4[1], v4[2]);
//    glEnd();
//
//
//
//}
