//
// Created by milo on 13/12/2020.
//

#ifndef COURSEWORK2_PLANE_H
#define COURSEWORK2_PLANE_H


#include "Shape.h"

class Plane  : public Shape{ //

    Q_OBJECT

public:
    Plane(QWidget *parent);
    Plane(QWidget *parent, float x_width, float y_depth, float z_height);
    void draw();

};
#endif //COURSEWORK2_PLANE_H
