//
// Created by milo on 30/11/2020.
//

#include "Pyramid.h"
#include <GL/glu.h>
#include <QGLWidget>
#include <QDebug>
#include "Pyramid.h"
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>


#include "../Materials/Material.cpp"
#include "Textures/Textures.h"

// constructor
Pyramid::Pyramid(QWidget *parent)
        : Shape(parent){ // constructor


} // constructor

// constructor
Pyramid::Pyramid(QWidget *parent, float x_width, float y_height, float z_depth)
        : Shape(parent, x_width, y_height, z_depth){  // constructor

} // constructor




void Pyramid::draw() {

    glPushMatrix();
    glTranslatef(0,this->y_height/2,0);
    glScalef(this->x_width/2, this->y_height/2, this->z_depth/2);

    glMaterialfv(GL_FRONT, GL_AMBIENT, this->material->ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, this->material->diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, this->material->specular);
    glMaterialf(GL_FRONT, GL_SHININESS, this->material->shininess);

    spotRotation();

    if (this->isShadow == true){
        glDisable(GL_LIGHTING);
        glDisable(GL_TEXTURE_2D);
        glColor3f(0.,0.,0.);
    }else{
        if (this->textureId == -1 || !Textures::isTextures1()){
            glDisable(GL_TEXTURE_2D);
        }
        else{
            this->enableTexture();
        }
    }



    float scale = 1.0;

    glm::vec3 v1 = {scale, -scale, -scale};
    glm::vec3 v2 = {scale, scale, -scale};
    glm::vec3 v3 = {0., 0., scale};

    glm::vec3 n = glm::normalize(glm::cross(v2 - v1, v3 - v2));

    glNormal3fv(glm::value_ptr(n));
    glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 0.0); glVertex3f(v1[0], v1[1], v1[2]);
    glTexCoord2f(rp.rx, 0.5*rp.ry); glVertex3f(v2[0], v2[1], v2[2]);
    glTexCoord2f(rp.rx, 0.0); glVertex3f(v3[0], v3[1], v3[2]);
    glEnd();

    v1 = {scale, scale, -scale};
    v2 = {-scale, scale, -scale};


    n = glm::normalize(glm::cross(v2 - v1, v3 - v2));

    glColor3f(1.0,.8,1.0);

    glNormal3fv(glm::value_ptr(n));
    glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 0.0); glVertex3f(v1[0], v1[1], v1[2]);
    glTexCoord2f(rp.rx, 0.5*rp.ry); glVertex3f(v2[0], v2[1], v2[2]);
    glTexCoord2f(rp.rx, 0.0);glVertex3f(v3[0], v3[1], v3[2]);
    glEnd();

    v1 = {-scale, scale, -scale};
    v2 = {-scale, -scale, -scale};

    n = glm::normalize(glm::cross(v2 - v1, v3 - v2));

    glColor3f(1.0,1.0,1.0);

    glNormal3fv(glm::value_ptr(n));
    glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 0.0);glVertex3f(v1[0], v1[1], v1[2]);
    glTexCoord2f(rp.rx, 0.5*rp.ry);glVertex3f(v2[0], v2[1], v2[2]);
    glTexCoord2f(rp.rx, 0.0);glVertex3f(v3[0], v3[1], v3[2]);
    glEnd();

    v1 = {-scale, -scale, -scale};
    v2 = {scale, -scale, -scale};

    n = glm::normalize(glm::cross(v2 - v1, v3 - v2));


    glColor3f(1.0,.8,1.0);

    glNormal3fv(glm::value_ptr(n));
    glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 0.0);glVertex3f(v1[0], v1[1], v1[2]);
    glTexCoord2f(rp.rx, 0.5*rp.ry);glVertex3f(v2[0], v2[1], v2[2]);
    glTexCoord2f(rp.rx, 0.0); glVertex3f(v3[0], v3[1], v3[2]);
    glEnd();

    glPopMatrix();
    glEnable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);
}


void Pyramid::setSpotRotation(void (*spotRotation)()) {
    Pyramid::spotRotation = spotRotation;
}

