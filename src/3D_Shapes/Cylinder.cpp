//
// Created by milo on 12/12/2020.
//

#include "Cylinder.h"
#include "Textures/Textures.h"
#include <cmath>
#include <iostream>
using namespace std;

// Play with these parameters
static const int N        = 50; // This determines the number of faces of the cylinder
static const int n_div   =  30;  // This determines the fineness of the cylinder along its length
static const float PI = 3.1415926535;



// constructor
Cylinder::Cylinder(QWidget *parent)
        : Shape(parent){ // constructor

} // constructor

// constructor
Cylinder::Cylinder(QWidget *parent, float x_width, float y_height, float z_depth)
        : Shape(parent, x_width, y_height, z_depth){ // constructor

} // constructor


void Cylinder::draw(){

    glPushMatrix();

    glTranslatef(0,this->y_height/2,0);
    glScalef(this->x_width/2, this->y_height/2, this->z_depth/2);

    switch(orientation)
    {
        case X  : glRotatef(90, 1,0,0); break;
        case Y: glRotatef(90, 0,1,0); break;
        case Z : break;
    }

    glMaterialfv(GL_FRONT, GL_AMBIENT,    this->material->ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,    this->material->diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,   this->material->specular);
    glMaterialf(GL_FRONT, GL_SHININESS,   this->material->shininess);

    if (this->isShadow == true){
        glDisable(GL_LIGHTING);
        glDisable(GL_TEXTURE_2D);
        glColor3f(0.,0.,0.);
    }else{
        if (this->textureId == -1 || !Textures::isTextures1()){
            glDisable(GL_TEXTURE_2D);
        }
        else{
            cout << "here baby" << endl;
            this->enableTexture();
        }
    }




    float x0, x1, y0, y1;

    float z_min = -1;
    float z_max =  1;

    float delta_z = (z_max - z_min)/n_div;

    for (int i = 0; i < N; i++){
        for(int i_z = 0; i_z < n_div; i_z++){
            x0 = cos(2*i*PI/N);
            x1 = cos(2*(i+1)*PI/N);
            y0 = sin(2*i*PI/N);
            y1 = sin(2*(i+1)*PI/N);


            float z = z_min + i_z*delta_z;


            glBegin(GL_POLYGON);
            glNormal3f(x0,y0,0);
            glTexCoord2f(static_cast<float>(i)/N, static_cast<float>(i_z)/n_div);
            glVertex3f(x0,y0,z);

            glNormal3f(x1,y1,0);
            glTexCoord2f(static_cast<float>(i+1)/N, static_cast<float>(i_z)/n_div);
            glVertex3f(x1,y1,z);

            glNormal3f(x1,y1,0);
            glTexCoord2f(static_cast<float>(i+1)/N, static_cast<float>(i_z+1)/n_div);
            glVertex3f(x1,y1,z+delta_z);

            glNormal3f(x0,y0,0);
            glTexCoord2f(static_cast<float>(i)/N, static_cast<float>(i_z+1)/n_div);
            glVertex3f(x0,y0,z+delta_z);
            glEnd();
        }
    }
    glEnable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);
    glPopMatrix();
}

void Cylinder::setOrientation(Orientation orientation) {
    Cylinder::orientation = orientation;
}
