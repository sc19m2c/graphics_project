//
// Created by milo on 13/12/2020.
//

#ifndef COURSEWORK2_JOINT_H
#define COURSEWORK2_JOINT_H

enum Axis { Xa, Ya, Za };

class Joint {

protected:
    float angle = 0;
    Axis axis = Xa;
public:
    Joint();
    Joint(float angle);
    void setAngle(float angle);
    float getAngle() const;
    Axis getAxis() const;
    void setAxis(Axis axis);

    void tranformAngle();
};


#endif //COURSEWORK2_JOINT_H
