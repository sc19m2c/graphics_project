//
// Created by milo on 30/11/2020.
//

#ifndef COURSEWORK2_WINDOW_H
#define COURSEWORK2_WINDOW_H


#include <QGLWidget>
#include <QMenuBar>
#include <QSlider>
#include <QBoxLayout>
#include "3D_Shapes/Pyramid.h"
#include "InterfaceController.h"


#include <QTimer>
#include <QComboBox>
#include <QDoubleSpinBox>
//#include "OrthoDemoWidget.h"


class Window : public QWidget {

public:


    // constructor / destructor
    Window(QWidget *parent);

    ~Window();

    // visual hierarchy
    // menu bar
    QMenuBar *menuBar;
    // file menu
    QMenu *fileMenu;
    // quit action
    QAction *actionQuit;


protected:
    // window layout
    QBoxLayout *windowLayout;

    // beneath that, the main widget
    InterfaceController *interface;
    // and a slider for the number of vertices
    QSlider *cameraSlider;

public:
    InterfaceController *getAnInterface() const;
    void setAnInterface(InterfaceController *anInterface);

    // a timer
    QTimer *ptimer;
    void ResetInterface();



};

#endif //COURSEWORK2_WINDOW_H
