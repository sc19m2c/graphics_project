//
// Created by milo on 30/11/2020.
//

#include "InterfaceController.h"
#include "3D_Shapes/Pyramid.h"
#include "3D_Shapes/Cube.h"
#include "3D_Shapes/Sphere.h"
#include "3D_Shapes/Cylinder.h"
#include "3D_Shapes/Plane.h"
#include "Groups/SimpleBot/SimpleBot.h"
#include "Groups/ComponentBot/CompBot.h"
//
// Created by milo on 30/11/2020.
//

#include <GL/glu.h>
#include <QGLWidget>
#include <QDebug>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Types/Offset.cpp"
#include "Groups/Humaniod/Humanoid.h"
#include "3D_Shapes/Textures/Textures.h"
#include "Groups/Cubemap/Cubemap.h"
#include "Groups/WorldMap/WorldMap.h"
#include "Groups/CenterPiece/CenterPiece.h"
#include "Materials/Material.cpp"
#include "3D_Shapes/ConvexShape.h"

#include <iostream>
#include <string>

using namespace std;

InterfaceController::~InterfaceController(){
    cout << "destroying interface controller" << endl;
    delete reg;
}

// constructor
InterfaceController::InterfaceController(QWidget *parent)
        : QGLWidget(parent),
          _ortho_par(-5.0, 5.0, -5.0, 5.0, -5.0, 5.0),
          _glupar(0.0,0.25,0.25, 0.0,0.0,0.0, 0.0,1.0,0.0),
          _b_lighting(true),
          light_pos{1.5, 5.5, 1., 1.},
          space({15,5,15}){ // constructor

    setFocusPolicy(Qt::StrongFocus);

    this->cam = new Camera(this);
    this->reg = new Register();

//    map<string, float> config;
//    config["base"] = -45.0;
//    config["lower_arm"] = -90.0;
//    config["upper_arm"] = 45.0;
//    KeyFrame *startKeyFrame = new KeyFrame(config, &defaultOffset, 0);
//    reg->addRobot("CompBot_1", startKeyFrame);
//    reg->getRobotAnimator("CompBot_1")->setPaths({new string("CompBot_1_SimpleKeyFrame.xml")});

    map<string, float> humanoidConfig;
    humanoidConfig["root"] = 0.0;
    humanoidConfig["neck"] = 0.0;
    humanoidConfig["head"] = 0.0;
    humanoidConfig["left_upper_arm"] = 90.0;
    humanoidConfig["left_lower_arm"] = 0.0;
    humanoidConfig["right_upper_arm"] = 90.0;
    humanoidConfig["right_lower_arm"] = 0.0;
    humanoidConfig["left_upper_leg"] = 180.0;
    humanoidConfig["left_lower_leg"] = 0.0;
    humanoidConfig["left_foot"] = -90.0;
    humanoidConfig["right_upper_leg"] = 130.0;
    humanoidConfig["right_lower_leg"] = 30.0;
    humanoidConfig["right_foot"] = -90.0;
    KeyFrame *h_startKeyFrame = new KeyFrame(humanoidConfig, new Offset({3.0, 2.5, 1.0}), 0);
    reg->addRobot("Humanoid_1", h_startKeyFrame);
    reg->getRobotAnimator("Humanoid_1")->setPaths({new string("Humanoid/RunningSequence.xml")});

    map<string, float> centerConfig;
    centerConfig["root"] = 0;
    centerConfig["topPyramid"] = 0;
    centerConfig["bottomPyramid"] = 0;
    KeyFrame *c_startKeyFrame = new KeyFrame(centerConfig, new Offset({0., 4.5, 0}),0);
    reg->addRobot("centerObject", c_startKeyFrame);




} // constructor

// called when OpenGL context is set up
void InterfaceController::initializeGL() { // initializeGL()
    // set the widget background colour

    glClearColor(0.3, 0.3, 0.3, 0.0);


    glEnable(GL_TEXTURE_2D);
    std::vector<std::string> faces={
            "./cubemap/right.png",
            "./cubemap/left.png",
            "./cubemap/top.png",
            "./cubemap/bottom.png",
            "./cubemap/front.png",
            "./cubemap/back.png"
    };

    std::vector<std::string> flatTextures={
            "./textures/required/globe.ppm",
            "./textures/required/prof.ppm",
    };

    std::vector<std::string> repeatTextures={
            "./textures/robot/robody.ppm",
            "./textures/robot/roboleg.ppm",
            "./textures/robot/roboleg2.ppm",
            "./textures/world/floorTile.ppm",
            "./textures/world/oldPlank.ppm",
            "./textures/world/paintedPlank.ppm",
            "./textures/world/bluePlasma.ppm",
            "./textures/robot/neck.ppm",

    };


    Textures *textures = new Textures();
    textures->loadBackgroundTextures(faces);
    textures->loadFlatTextures(flatTextures);
    textures->loadRepeatTextures(repeatTextures);


//    glMatrixMode(GL_PROJECTION);
//    cam->perspective();

} // initializeGL()


// called every time the widget is resized
void InterfaceController::resizeGL(int w, int h) { // resizeGL()
    // set the viewport to the entire widget
    glViewport(0, 0, w, h);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glEnable(GL_LIGHTING); // enable lighting in general
    glEnable(GL_LIGHT0);   // each light source must also be enabled

    this->setLighting();
//    glLightfv(GL_LIGHT0, GL_POSITION, this->light_pos);
//    glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 180.);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    cam->perspective();


} // resizeGL()

void InterfaceController::setLighting() {

    //floor
    for (int i = 0; i < 16; i++)this->projectionMatrix[i] = 0.0;
    this->projectionMatrix[0] = this->projectionMatrix[5] = this->projectionMatrix[10] = 1.0;
    this->projectionMatrix[7] = -1.0 / this->light_pos[1];

    //left
    float leftOffset = (space.x+light_pos[0])/light_pos[0];
    for (int i = 0; i < 16; i++)this->projectionMatrixLeft[i] = 0.0;
    this->projectionMatrixLeft[0] = this->projectionMatrixLeft[5] = this->projectionMatrixLeft[10] = leftOffset;
    this->projectionMatrixLeft[3] = -1.0 / this->light_pos[0];

    //right
    float rightOffset = (space.x-light_pos[0])/light_pos[0];
    for (int i = 0; i < 16; i++)this->projectionMatrixRight[i] = 0.0;
    this->projectionMatrixRight[0] = this->projectionMatrixRight[5] = this->projectionMatrixRight[10] = rightOffset;
    this->projectionMatrixRight[3] = 1.0 / this->light_pos[0];

    //front
    float frontOffset = (space.z+light_pos[2])/light_pos[2];
    for (int i = 0; i < 16; i++)this->projectionMatrixFront[i] = 0.0;
    this->projectionMatrixFront[0] = this->projectionMatrixFront[5] = this->projectionMatrixFront[10] = frontOffset;
    this->projectionMatrixFront[11] = -1.0 / this->light_pos[2];

//    glPopMatrix();
}


void InterfaceController::clipShadows(){
    float delta = 0.1;
    //floor
    glEnable( GL_CLIP_PLANE0);
    GLdouble eq[4] = {0.0, 1.0, 0.0, delta};
    glClipPlane(GL_CLIP_PLANE0, eq);

    //left
    glEnable( GL_CLIP_PLANE1);
    GLdouble eq1[4] = {1.0, 0.0, 0.0, space.x+delta};
    glClipPlane(GL_CLIP_PLANE1, eq1);

//    right
    glEnable( GL_CLIP_PLANE2);
    GLdouble eq2[4] = {-1.0, 0.0, 0.0, space.x+delta};
    glClipPlane(GL_CLIP_PLANE2, eq2);

    //front
    glEnable( GL_CLIP_PLANE3);
    GLdouble eq3[4] = {0.0, 0.0, 1.0, space.z+delta};
    glClipPlane(GL_CLIP_PLANE3, eq3);


}


void InterfaceController::projectShadows() {
    //floor
    if(this->state.isClipShadows){
        clipShadows();
    }


    glPushMatrix();
    glPushAttrib(GL_CURRENT_BIT);

    glTranslatef(this->light_pos[0], this->light_pos[1], this->light_pos[2]);
    glMultMatrixf(this->projectionMatrix);
    glTranslatef(-this->light_pos[0], -this->light_pos[1], -this->light_pos[2]);

    glColor3f(0., 0., 0.);
    this->drawSceneInterior(true);
    glPopMatrix();
    glPopAttrib();

    //left
    glPushMatrix();
    glPushAttrib(GL_CURRENT_BIT);

    glTranslatef(this->light_pos[0], this->light_pos[1], this->light_pos[2]);
    glMultMatrixf(this->projectionMatrixLeft);
    glTranslatef(-this->light_pos[0], -this->light_pos[1], -this->light_pos[2]);

    glColor3f(0., 0., 0.);
    this->drawSceneInterior(true);
    glPopMatrix();
    glPopAttrib();

    //right
    glPushMatrix();
    glPushAttrib(GL_CURRENT_BIT);

    glTranslatef(this->light_pos[0], this->light_pos[1], this->light_pos[2]);
    glMultMatrixf(this->projectionMatrixRight);
    glTranslatef(-this->light_pos[0], -this->light_pos[1], -this->light_pos[2]);

    glColor3f(0., 0., 0.);
    this->drawSceneInterior(true);
    glPopMatrix();
    glPopAttrib();

    //front
    glPushMatrix();
    glPushAttrib(GL_CURRENT_BIT);

    glTranslatef(this->light_pos[0], this->light_pos[1], this->light_pos[2]);
    glMultMatrixf(this->projectionMatrixFront);
    glTranslatef(-this->light_pos[0], -this->light_pos[1], -this->light_pos[2]);

    glColor3f(0., 0., 0.);
    this->drawSceneInterior(true);
    glPopMatrix();
    glPopAttrib();

    glDisable( GL_CLIP_PLANE0);
    glDisable( GL_CLIP_PLANE1);
    glDisable( GL_CLIP_PLANE2);
    glDisable( GL_CLIP_PLANE3);
}

void InterfaceController::drawSceneBoundary() {

    Cubemap *cubemap = new Cubemap(this);
    WorldMap *world = new WorldMap(this, &space);
    CenterPiece *center = new CenterPiece(this, "centerObject");




    if(this->state.isCubemap){
        glPushMatrix();
            glDepthMask(GL_FALSE);
            glScalef(1000,1000,1000);
            cubemap->draw();
            glDepthMask(GL_TRUE);
        glPopMatrix();
    }

    if(this->state.isWorldmap){
        glPushMatrix();
            world->draw();
        glPopMatrix();
    }

    if(this->state.isCenter){
        glPushMatrix();
            center->setIsBright(true);
            center->initialise();
            center->initTranformation();
            center->draw();
        glPopMatrix();
    }


//    glEnable(GL_CULL_FACE);
//    glCullFace(GL_BACK);
//    glFrontFace(GL_CW);


    delete cubemap;
    delete center;
    delete world;


}

void InterfaceController::drawSceneInterior(bool isShadow) {

    Humanoid *human = new Humanoid(this, "Humanoid_1");
    ConvexShape *conv = new ConvexShape(this);
    if (isShadow){
        human->set_isShadow(isShadow);
        conv->set_isShadow(isShadow);
    }

    glPushMatrix();
        glTranslatef(13,3,-13);
        glScalef(2,2,2);
        glRotatef(this->currentTime/100,0,1,0);
        conv->set_material(&demo2);
        conv->draw();
    glPopMatrix();

    if(this->state.isHumanoid){
        glPushMatrix();
            human->initialise();
            human->initTranformation();
            human->draw();
        glPopMatrix();
    }

    delete human;
    delete conv;

}

// called every time the widget needs painting
void InterfaceController::paintGL() { // paintGL()
    // clear the widget

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    // testing new stuff
    // You must set the matrix mode to model view directly before enabling the depth test
    glMatrixMode(GL_MODELVIEW);
    glEnable(GL_DEPTH_TEST);
    cam->lookAt();
    glLightfv(GL_LIGHT0, GL_POSITION, this->light_pos);
    glLightf(GL_LIGHT0,GL_SPOT_CUTOFF,180.0f);

    glEnable(GL_NORMALIZE);
    glShadeModel(GL_SMOOTH);


//    this->setLighting();
    this->drawSceneInterior();

    if(this->state.isShadows){
        this->projectShadows();
    }
    this->drawSceneBoundary();

    glFlush();

}

void InterfaceController::mouseMoveEvent(QMouseEvent *event){
    cam->doPan(event);
}
void InterfaceController::mousePressEvent(QMouseEvent *event){
    cam->startPan(event);
}
void InterfaceController::keyPressEvent(QKeyEvent *event){
    cam->moveCam(event);
}




//  --------------------------  slots -------------------------

void InterfaceController::setGLU_x(int x) {
    _glupar._x = (float(x)/30);
    update();
}

void InterfaceController::update_positions() {
    this->currentTime += 500;

    map<string, Animation*> robots = this->reg->getAnimationReg();
    for(auto robot = robots.begin(); robot != robots.end(); robot++){
        Animation* a = robot->second;
        a->animate(this->currentTime, this->reg, this);
    }

    update();

}

Register *InterfaceController::getReg() const {
    return reg;
}

void InterfaceController::setReg(Register *reg) {
    InterfaceController::reg = reg;
}

int InterfaceController::getCurrentTime() const {
    return currentTime;
}

void InterfaceController::setCurrentTime(int currentTime) {
    InterfaceController::currentTime = currentTime;
}

const AppState &InterfaceController::getState() const {
    return state;
}

void InterfaceController::setState(const AppState &state) {
    InterfaceController::state = state;
}
