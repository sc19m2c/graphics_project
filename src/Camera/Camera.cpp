//
// Created by milo on 22/12/2020.
//

#include <GL/glu.h>
#include "Camera.h"
#include "../InterfaceController.h"

Camera::Camera(InterfaceController *interface) {
    this->interface = interface;

    cameraPos = glm::vec3(0.0f, 2.0f, 5.0f);

    cameraFront = glm::vec3(0.0476016, -0.338738, -0.939676);
    cameraAt = cameraPos + cameraFront;

    cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);
};

void Camera::perspective() {


    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glFrustum(-1., 1., -1., 1., 1.5, 3000.0);

    glMatrixMode(GL_MODELVIEW);
}

void Camera::lookAt() {
    glLoadIdentity();

    cameraAt = cameraPos + cameraFront;

    gluLookAt(cameraPos[0], cameraPos[1], cameraPos[2],
              cameraAt[0], cameraAt[1], cameraAt[2],
              cameraUp[0], cameraUp[1], cameraUp[2]);
}


void Camera::moveCam(QKeyEvent *event){
    std::string key = event->text().toStdString();
    float cameraSpeed = .1f;

    if (key.compare("w") == 0){
        cameraPos += cameraSpeed * cameraFront;
    }
    if (key.compare("s") == 0){
        cameraPos -= cameraSpeed * cameraFront;
    }
    if (key.compare("a") == 0){
        cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
    }
    if (key.compare("d") == 0){
        cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
    }
    interface->update();
}

void Camera::startPan(QMouseEvent *event){
    lastX = event->pos().x();
    lastY = event->pos().y();
}
void Camera::doPan(QMouseEvent *event){
    float xpos = event->pos().x();
    float ypos = event->pos().y();

    float xoffset = lastX - xpos ;
    float yoffset =  ypos - lastY ;
    lastX = xpos;
    lastY = ypos;


    float sensitivity = 0.1f;
    xoffset *= sensitivity;
    yoffset *= sensitivity;


    yaw   += xoffset;
    pitch += yoffset;

    if(pitch > 89.0f)
        pitch = 89.0f;
    if(pitch < -89.0f)
        pitch = -89.0f;

    glm::vec3 direction;
    direction.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
    direction.y = sin(glm::radians(pitch));
    direction.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
    cameraFront = glm::normalize(direction);

    interface->update();
}

