//
// Created by milo on 22/12/2020.
//

#ifndef COURSEWORK2_CAMERA_H
#define COURSEWORK2_CAMERA_H
#include <glm/glm.hpp>
#include<QKeyEvent>
#include <QGLWidget>

class InterfaceController;

class Camera :QGLWidget {

public:
    Camera(InterfaceController *interface);
    void lookAt();
    void perspective();
    void moveCam(QKeyEvent *event);
    void startPan(QMouseEvent *event);
    void doPan(QMouseEvent *event);

protected:
    InterfaceController *interface;

    glm::vec3 cameraPos;
    glm::vec3 cameraFront;
    glm::vec3 cameraAt;
    glm::vec3 cameraUp;

    float lastX;
    float lastY ;
    float yaw = -87.1 ;
    float pitch =  -19.8;

};


#endif //COURSEWORK2_CAMERA_H
