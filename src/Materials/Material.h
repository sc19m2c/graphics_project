//
// Created by milo on 12/12/2020.
//

#ifndef COURSEWORK2_MATERIAL_H
#define COURSEWORK2_MATERIAL_H
#include <GL/glu.h>

class Material {

};

typedef struct materialStruct {
    GLfloat ambient[4];
    GLfloat diffuse[4];
    GLfloat specular[4];
    GLfloat shininess;
} materialStruct;

#endif //COURSEWORK2_MATERIAL_H
