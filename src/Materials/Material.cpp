//
// Created by milo on 12/12/2020.
//


#include <GL/glu.h>
#include "Material.h"


static materialStruct brassMaterials = {
        {0.33, 0.22, 0.03, 1.0},
        {0.58, 0.57, 0.11, 1.0},
        {1.99, 1.91, 1.81, 1.0},
        100.8
};

static materialStruct whiteShinyMaterials = {
        {1.0, 1.0, 1.0, 1.0},
        {1.0, 1.0, 1.0, 1.0},
        {1.0, 1.0, 1.0, 1.0},
        100.0
};


static materialStruct blueShinyMaterials = {
        {0.0, 0.0, 0.5, .0},
        {0.0, 0.0, 0.7, .0},
        {0.0, 0.0, 1.0, .0},
        100.0
};

static materialStruct demo1 = {
        {1.0f, 0.5f, 0.31f, 1.0},
        {1.0f, 0.5f, 0.31f, .0},
        {0.5f, 0.5f, 0.5f, .0},
        2.0f
};

static materialStruct demo2 = {
        {.9f, 0.1f, 0.1f, 1.0},
        {.9f, 0.1f, 0.1f, 1.0},
        {1.f, 01.0f, 1.f, 1.0},
        100.0f
};


static materialStruct greenShinyMaterials = {
        {0.0, 0.0, 1.0, 0.0},
        {1.0, 1.0, 1.0, 1.0},
        {0.0, 0.0, 0.0, 0.0},
        100.0
};


static materialStruct redShinyMaterials = {
        {1.0, 0.0, 0.0, 0.0},
        {1.0, 0.0, 0.0, 0.0},
        {1.0, 0.0, 0.0, 0.0},
        100.0
};