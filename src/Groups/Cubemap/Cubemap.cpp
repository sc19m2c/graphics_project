//
// Created by milo on 29/12/2020.
//

#include "Cubemap.h"
#include "../../3D_Shapes/Textures/Textures.h"

Cubemap::~Cubemap() {}

Cubemap::Cubemap(QWidget *parent):QGLWidget(parent) {

}

void Cubemap::draw(){

    glPushMatrix();
    glDisable(GL_LIGHTING);

    glColor3f(1.0, 1.0, 1.0);

    if ( !Textures::isTextures1()){
        glEnable(GL_LIGHTING);
        glDisable(GL_TEXTURE_2D);
        glColor3f(0.2, 0.2, 0.2);
    }

    GLfloat normals[][3] = {{-1.,  0., 0.},
                            {1., 0., 0.},
                            {0.,  0., -1.},
                            {0.,  0., 1.},
                            {0,   -1,  0},
                            {0,   1, 0}};

    glBindTexture(GL_TEXTURE_2D, Textures::backgroundTextures[1]);

    //right
    glNormal3fv(normals[0]);
    glBegin(GL_POLYGON);

    glTexCoord2f(1.0, 0.0); glVertex3f(1.0, -1.0, 1.0);
    glTexCoord2f(0.0, 0.0); glVertex3f(1.0, -1.0, -1.0);
    glTexCoord2f(0.0, 1.0); glVertex3f(1.0, 1.0, -1.0);
    glTexCoord2f(1.0, 1.0); glVertex3f(1.0, 1.0, 1.0);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, Textures::backgroundTextures[0]);
    //left
    glNormal3fv(normals[1]);
    glBegin(GL_POLYGON);

    glTexCoord2f(0.0, 0.0);glVertex3f(-1.0, -1.0, 1.0);
    glTexCoord2f(1.0, 0.0);glVertex3f(-1.0, -1.0, -1.0);
    glTexCoord2f(1.0, 1.0);glVertex3f(-1.0, 1.0, -1.0);
    glTexCoord2f(0.0, 1.0);glVertex3f(-1.0, 1.0, 1.0);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, Textures::backgroundTextures[2]);

    //top
    glNormal3fv(normals[4]);
    glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 1.0);glVertex3f(1.0, 1.0, 1.0);
    glTexCoord2f(1.0, 1.0);glVertex3f(1.0, 1.0, -1.0);
    glTexCoord2f(1.0, 0.0);glVertex3f(-1.0, 1.0, -1.0);
    glTexCoord2f(0.0, 0.0);glVertex3f(-1.0, 1.0, 1.0);

    glEnd();

    glBindTexture(GL_TEXTURE_2D, Textures::backgroundTextures[3]);

    //bottom
    glNormal3fv(normals[5]);
    glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 0.0);glVertex3f(1.0, -1.0, 1.0);
    glTexCoord2f(1.0, 0.0);glVertex3f(1.0, -1.0, -1.0);
    glTexCoord2f(1.0, 1.0);glVertex3f(-1.0, -1.0, -1.0);
    glTexCoord2f(0.0, 1.0);glVertex3f(-1.0, -1.0, 1.0);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, Textures::backgroundTextures[4]);

    //front
    glNormal3fv(normals[3]);
    glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 0.0);glVertex3f(-1.0, -1.0, -1.0);
    glTexCoord2f(1.0, 0.0);glVertex3f(1.0, -1.0, -1.0);
    glTexCoord2f(1.0, 1.0);glVertex3f(1.0, 1.0, -1.0);
    glTexCoord2f(0.0, 1.0);glVertex3f(-1.0, 1.0, -1.0);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, Textures::backgroundTextures[5]);

    //back
    glNormal3fv(normals[2]);
    glBegin(GL_POLYGON);
    glTexCoord2f(1.0, 0.0);glVertex3f(-1.0, -1.0, 1.0);
    glTexCoord2f(0.0, 0.0);glVertex3f(1.0, -1.0, 1.0);
    glTexCoord2f(0.0, 1.0);glVertex3f(1.0, 1.0, 1.0);
    glTexCoord2f(1.0, 1.0);glVertex3f(-1.0, 1.0, 1.0);
    glEnd();
    glEnable(GL_LIGHTING); // enable lighting in general

    glPopMatrix();
    glEnable(GL_TEXTURE_2D);
}