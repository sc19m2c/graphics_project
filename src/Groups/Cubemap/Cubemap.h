//
// Created by milo on 29/12/2020.
//

#ifndef COURSEWORK2_CUBEMAP_H
#define COURSEWORK2_CUBEMAP_H


#include <QGLWidget>

class Cubemap: public QGLWidget{

public:
    Cubemap(QWidget *parent);
    ~Cubemap();
    void draw();
};


#endif //COURSEWORK2_CUBEMAP_H
