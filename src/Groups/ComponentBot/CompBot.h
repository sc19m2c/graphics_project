//
// Created by milo on 13/12/2020.
//

#ifndef COURSEWORK2_COMPBOT_H
#define COURSEWORK2_COMPBOT_H


#include "../Component.h"

class CompBot : public Component{

public:
    CompBot(QWidget *parent, string name);
    virtual void initialise();
};


#endif //COURSEWORK2_COMPBOT_H
