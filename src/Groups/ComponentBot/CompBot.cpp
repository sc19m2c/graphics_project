//
// Created by milo on 13/12/2020.
//

#include <iostream>
#include "CompBot.h"
#include "Components/Base/Base.h"


CompBot::CompBot(QWidget *parent, string name):Component(parent,name){
    this->root = this;
    this->y_height = 0;
    this->x_width = 0;
    this->z_depth = 0;

}

void CompBot::initialise(){

    Base *base = new Base(this,"base", 1,1,1);
    base->setAngle(this->configuration.at(base->getName()));
    base->setAxis(Ya);
    this->children.push_back(base);
//    this->siblings.push_back(rightArm);
}

