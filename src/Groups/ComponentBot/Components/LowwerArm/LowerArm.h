//
// Created by milo on 13/12/2020.
//

#ifndef COURSEWORK2_LOWERARM_H
#define COURSEWORK2_LOWERARM_H


#include "../../../Component.h"

class LowerArm : public Component{

public:
    LowerArm(QWidget *parent, string name, float x_width, float y_height, float z_depth );
    virtual void initialise();
};

#endif //COURSEWORK2_LOWERARM_H
