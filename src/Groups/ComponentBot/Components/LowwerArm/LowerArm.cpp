//
// Created by milo on 13/12/2020.
//

#include "LowerArm.h"
#include "../../../../3D_Shapes/Cube.h"
#include "../UpperArm/UpperArm.h"


LowerArm::LowerArm(QWidget *parent,string name, float x_width, float y_height, float z_depth ):
        Component(parent,name, x_width, y_height, z_depth){

}

void LowerArm::initialise(){

    Cube *lower_arm = new Cube(nullptr, 0.5, 2, 0.5);
    lower_arm->set_isShadow(this->isShadow);
    this->objects.push_back(lower_arm);

    UpperArm *upperArm = new UpperArm(this, "upper_arm", 0.5, 1, 0.5);
    upperArm->setAngle(this->root->getConfiguration().at(upperArm->getName()));
    upperArm->setAxis(Za);
    this->children.push_back(upperArm);
}