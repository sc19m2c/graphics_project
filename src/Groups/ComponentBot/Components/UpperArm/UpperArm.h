//
// Created by milo on 13/12/2020.
//

#ifndef COURSEWORK2_HUMANUPPERARM_H
#define COURSEWORK2_UPPERARM_H


#include "../../../Component.h"

class UpperArm : public Component{

public:
    UpperArm(QWidget *parent, string name, float x_width, float y_height, float z_depth );
    virtual void initialise();
};


#endif //COURSEWORK2_HUMANUPPERARM_H
