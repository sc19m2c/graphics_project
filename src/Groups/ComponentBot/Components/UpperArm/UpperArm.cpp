//
// Created by milo on 13/12/2020.
//

#include "UpperArm.h"
#include "../../../../3D_Shapes/Cube.h"

UpperArm::UpperArm(QWidget *parent, string name, float x_width, float y_height, float z_depth ):
        Component(parent,name, x_width, y_height, z_depth){

}

void UpperArm::initialise(){

    Cube *upper_arm = new Cube(nullptr,  0.5, 1, 0.5);
    upper_arm->set_isShadow(this->isShadow);
    this->objects.push_back(upper_arm);

}