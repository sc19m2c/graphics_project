//
// Created by milo on 13/12/2020.
//

#ifndef COURSEWORK2_BASE_H
#define COURSEWORK2_BASE_H


#include "../../../Component.h"

class Base : public Component{

public:
    Base(QWidget *parent, string name, float x_width, float y_height, float z_depth );
    virtual void initialise();
};


#endif //COURSEWORK2_BASE_H
