//
// Created by milo on 13/12/2020.
//

#include "Base.h"
#include "../../../../3D_Shapes/Cylinder.h"
#include "../LowwerArm/LowerArm.h"

Base::Base(QWidget *parent,string name, float x_width, float y_height, float z_depth ):
    Component(parent,name, x_width, y_height, z_depth){
}

void Base::initialise(){


    Cylinder *base = new Cylinder(nullptr, 1,1,1);
    base->set_isShadow(this->isShadow);
    this->objects.push_back(base);

    LowerArm *lowerArm = new LowerArm(this,"lower_arm", 0.5, 2, 0.5);
    lowerArm->setAngle(this->root->getConfiguration().at(lowerArm->getName()));
    lowerArm->setAxis(Za);
    this->children.push_back(lowerArm);

}
