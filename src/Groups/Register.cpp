//
// Created by milo on 16/12/2020.
//

#include "Register.h"

#include<iostream>
#include "../Types/Offset.cpp"

using namespace std;

Register::Register(){

}



void Register::addRobot(string name, KeyFrame* startKeyFrame ){
    this->reg[name] = startKeyFrame->getConfig();
    Animation *animator = new Animation(name, startKeyFrame);
    this->animationReg[name] = animator;
    this->positionReg[name] = startKeyFrame->getPos();
}

map<string, float> Register::getRobotConfig(string name) {
    return this->reg.at(name);
}

void Register::setRobotConfig(string name, map<string, float> &config) {
    this->reg[name] = config;
}

Animation* Register::getRobotAnimator(string name){
    return this->animationReg[name];
}

void Register::setRobotPosition(string name, Offset* pos){
    this->positionReg[name] = pos;
}
Offset* Register::getRobotPosition(string name){
    return this->positionReg[name];
}

void Register::print(){
    for (auto robot = reg.begin(); robot != reg.end(); ++robot){
        map<string, float> config = robot->second;
        for (auto comp = config.begin(); comp != config.end(); ++comp){
            cout << robot->first << " :: "<< comp->first << " :: " << comp->second << endl;
        }
        cout << robot->first << " ::  x ::  "<< this->positionReg[robot->first]->offset[0] << " :: y :: " << this->positionReg[robot->first]->offset[1] << endl;
    }

}

const map<string, map<string, float>> &Register::getReg() const {
    return reg;
}

void Register::setReg(const map<string, map<string, float>> &reg) {
    Register::reg = reg;
}

const map<string, Animation *> &Register::getAnimationReg() const {
    return animationReg;
}

void Register::setAnimationReg(const map<string, Animation *> &animationReg) {
    Register::animationReg = animationReg;
}

const map<string, Offset *> &Register::getPositionReg() const {
    return positionReg;
}

void Register::setPositionReg(const map<string, Offset *> &positionReg) {
    Register::positionReg = positionReg;
}

