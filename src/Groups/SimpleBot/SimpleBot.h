//
// Created by milo on 13/12/2020.
//

#ifndef COURSEWORK2_SIMPLEBOT_H
#define COURSEWORK2_SIMPLEBOT_H


#include <QGLWidget>
#include "../../InterfaceController.h"

class SimpleBot : public QGLWidget {
Q_OBJECT
public:
    SimpleBot(QWidget *parent, InterfaceController *controller);
    void draw();

private:
    InterfaceController *parent;
};


#endif //COURSEWORK2_SIMPLEBOT_H
