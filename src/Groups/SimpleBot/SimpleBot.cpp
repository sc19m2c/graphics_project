//
// Created by milo on 13/12/2020.
//

#include <iostream>
#include "SimpleBot.h"
#include "../../3D_Shapes/Cylinder.h"
#include "../../3D_Shapes/Cube.h"
#include "../../3D_Shapes/Joint.h"
#include <iostream>
using namespace std;
// constructor
SimpleBot::SimpleBot(QWidget *parent, InterfaceController *controller)
        : QGLWidget(parent){ // constructor
    this->parent = controller;
} // constructor

void SimpleBot::draw() {

    //base -> lower arm -> upper arm


    Cylinder *base = new Cylinder(this, 1,1,1);
//    base->setAngle(this->parent->getCounter());
    base->setAxis(Ya);

    Cube *lower_arm = new Cube(this, 0.5, 2, 0.5);
//    lower_arm->setAngle(this->parent->getCounter()*1.5);
    lower_arm->setAxis(Za);
    lower_arm->setParentShape(base);

    Cube *upper_arm = new Cube(this,  0.5, 1, 0.5);
    upper_arm->setOffset(new Offset({1.,0.,0.,}));
//    upper_arm->setAngle(this->parent->getCounter()*2.);
    upper_arm->setAxis(Za);
    upper_arm->setParentShape(lower_arm);

    Shape *joints[3] = {base, lower_arm, upper_arm};

    for(Shape *j : joints){
        j->initTranformation();
        j->draw();
    }




//    glRotatef(this->parent->getCounter(),0,1,0);
//
//    glPushMatrix();
//        base->draw();
//    glPopMatrix();
//
//
//    glTranslatef(0,1,0);
//    glRotatef(-45,0,0,1);
//
//    glPushMatrix();
//        lower_arm->draw();
//    glPopMatrix();
//
//    glTranslatef(0,2,0);
//    glRotatef(-45,0,0,1);
//
//    glPushMatrix();
//        upper_arm->draw();
//    glPopMatrix();

}