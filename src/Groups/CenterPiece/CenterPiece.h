//
// Created by milo on 30/12/2020.
//

#ifndef COURSEWORK2_CENTERPIECE_H
#define COURSEWORK2_CENTERPIECE_H


#include "../Component.h"

class CenterPiece : public Component{

public:
    CenterPiece(QWidget *parent, string name);
    virtual void initialise();
};


#endif //COURSEWORK2_CENTERPIECE_H
