//
// Created by milo on 30/12/2020.
//

#include "Globe.h"
#include "../../../3D_Shapes/Sphere.h"
#include "../../../3D_Shapes/Textures/Textures.h"
#include "../Reusable/CenterPyramid/CenterPyramid.h"

Globe::Globe(QWidget *parent,string name, float x_width, float y_height, float z_depth ):
        Component(parent,name, x_width, y_height, z_depth){
}

void globeSpotRotation(){
    glRotatef(90,1,0,0);
}

void Globe::initialise(){

    Sphere *sphere = new Sphere(nullptr, 2.5, 2.5, 2.5);
    sphere->set_isShadow(this->isShadow);
    sphere->setTextureId(Textures::flatMaterialTextures[0]);
    sphere->setSpotRotation(globeSpotRotation);
    sphere->setAxis(Ya);
    sphere->setAngle(-this->root->getAnInterface()->getCurrentTime()/20);
    this->objects.push_back(sphere);

    CenterPyramid *top = new CenterPyramid(this, "topPyramid", 1.5,1.5,1.5);
    top->set_isShadow(this->isShadow);
    top->setAxis(Ya);
    top->setAngle(this->root->getAnInterface()->getCurrentTime()/100);
    top->setOffset(new Offset({0., 0.2, 0.}));
    this->children.push_back(top);

    CenterPyramid *bottom = new CenterPyramid(this, "bottomPyramid", 1.5,1.5,1.5);
    bottom->set_isShadow(this->isShadow);
    bottom->setStartFace(StartFace::NEG_Y);
    bottom->setAxis(Ya);
    bottom->setTop(0.);
    bottom->setAngle(this->root->getAnInterface()->getCurrentTime()/100);
    bottom->setOffset(new Offset({0., -1.7, 0.}));
    this->children.push_back(bottom);

}