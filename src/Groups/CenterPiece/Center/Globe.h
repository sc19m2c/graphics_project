//
// Created by milo on 30/12/2020.
//

#ifndef COURSEWORK2_GLOBE_H
#define COURSEWORK2_GLOBE_H


#include "../../Component.h"

class Globe : public Component{

public:
    Globe(QWidget *parent, string name, float x_width, float y_height, float z_depth );
    virtual void initialise();
};


#endif //COURSEWORK2_GLOBE_H
