//
// Created by milo on 30/12/2020.
//

#include "CenterPiece.h"
#include "Center/Globe.h"

CenterPiece::CenterPiece(QWidget *parent, string name): Component(parent, name){
    this->root = this;
    this->y_height = 0;
    this->x_width = 0;
    this->z_depth = 0;

}

void CenterPiece::initialise(){

    Globe *globe = new Globe(this,"root", 2.5, 2.5, 2.5);

    this->children.push_back(globe);




//    this->siblings.push_back(rightArm);
}