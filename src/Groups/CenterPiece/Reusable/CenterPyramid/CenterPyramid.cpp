//
// Created by milo on 30/12/2020.
//

#include "CenterPyramid.h"
#include "../../../../3D_Shapes/Pyramid.h"
#include "../../../../3D_Shapes/Textures/Textures.h"

CenterPyramid::CenterPyramid(QWidget *parent, string name, float x_width, float y_height, float z_depth ):
        Component(parent,name, x_width, y_height, z_depth){

}

void pyramidSpotRotation(){
    glRotatef(-90.,1,0,0);
}

void pyramidSpotRotation2(){
    glRotatef(90.,1,0,0);
}

void CenterPyramid::initialise(){


    Pyramid *pyramid = new Pyramid(nullptr, 1.5,1.5,1.5);
    pyramid->set_isShadow(this->isShadow);
    pyramid->setSpotRotation(top == 1 ? pyramidSpotRotation : pyramidSpotRotation2);
    pyramid->setTextureId(Textures::repeatMaterialTextures[6]);
    pyramid->setRp({3.,3.});

    this->objects.push_back(pyramid);

}

float CenterPyramid::getTop() const {
    return top;
}

void CenterPyramid::setTop(float top) {
    CenterPyramid::top = top;
}
