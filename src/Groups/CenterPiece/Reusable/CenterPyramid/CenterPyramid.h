//
// Created by milo on 30/12/2020.
//

#ifndef COURSEWORK2_CENTERPYRAMID_H
#define COURSEWORK2_CENTERPYRAMID_H


#include "../../../Component.h"

class CenterPyramid : public Component{

public:
    CenterPyramid(QWidget *parent, string name, float x_width, float y_height, float z_depth );
    virtual void initialise();

protected:
    float top = 1.0;
public:
    float getTop() const;

    void setTop(float top);

protected:
    void spotRotation();

};


#endif //COURSEWORK2_CENTERPYRAMID_H
