//
// Created by milo on 30/12/2020.
//

#ifndef COURSEWORK2_WORLDMAP_H
#define COURSEWORK2_WORLDMAP_H


#include <QGLWidget>
#include "../Component.h"

class WorldMap: public QGLWidget{

public:
    WorldMap(QWidget *parent, WorldSpace *map);
    ~WorldMap();
    void draw();

protected:
    WorldSpace *map;

};



#endif //COURSEWORK2_WORLDMAP_H
