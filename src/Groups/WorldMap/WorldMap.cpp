//
// Created by milo on 30/12/2020.
//

#include "WorldMap.h"
#include "../../3D_Shapes/Plane.h"
#include "../../3D_Shapes/Textures/Textures.h"


WorldMap::~WorldMap() {}

WorldMap::WorldMap(QWidget *parent, WorldSpace *map):QGLWidget(parent) {
    this->map = map;
}

void WorldMap::draw(){
    float delta = 0.01;
    Plane *floor = new Plane(this);
    Plane *leftWall = new Plane(this);
    Plane *rightWall = new Plane(this);
    Plane *frontWall = new Plane(this);

    glPushMatrix();
        glTranslatef(0, -delta,0);
        glScalef(map->x, 0, map->z);
        floor->setTextureId(Textures::repeatMaterialTextures[3]);
        floor->setRp({map->x/2, map->z/2});
        floor->draw();
    glPopMatrix();

    glPushMatrix();
        glTranslatef(-map->x-delta, map->y-delta,0);
        glRotatef(90.0, 0,0,1);
        glScalef(map->y+delta, 0, map->x+delta);
        leftWall->setTextureId(Textures::repeatMaterialTextures[4]);
        leftWall->draw();
    glPopMatrix();

    glPushMatrix();
        glTranslatef(map->x+delta, map->y-delta,0);
        glRotatef(-90.0, 0,0,1);
        glScalef(map->y+delta, 0, map->x+delta);
        rightWall->setTextureId(Textures::repeatMaterialTextures[4]);
        rightWall->draw();
    glPopMatrix();

    glPushMatrix();
        glTranslatef(0, map->y-delta,-map->z-delta);
        glRotatef(90.0, 1,0,0);
        glScalef(map->x+delta, 0, map->y+delta);
        frontWall->setTextureId(Textures::repeatMaterialTextures[5]);
        frontWall->setRp({2, 1});
        frontWall->draw();
    glPopMatrix();

    delete floor;
    delete leftWall;
    delete rightWall;
    delete frontWall;
}