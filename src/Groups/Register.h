//
// Created by milo on 16/12/2020.
//

#ifndef COURSEWORK2_REGISTER_H
#define COURSEWORK2_REGISTER_H
#include <map>
#include "../Animation/Animation.h"
#include "../Types/Offset.h"



using namespace std;



class Register {

protected:
    map<string, map<string, float>> reg;
    map<string, Offset*> positionReg;
    map<string, Animation*> animationReg;

public:
    const map<string, Animation *> &getAnimationReg() const;

    void setAnimationReg(const map<string, Animation *> &animationReg);

    const map<string, map<string, float>> &getReg() const;

    void setReg(const map<string, map<string, float>> &reg);

    const map<string, Offset *> &getPositionReg() const;

    void setPositionReg(const map<string, Offset *> &positionReg);

public:
    Register();

    void addRobot(string name, KeyFrame* startKeyFrame);

    void setRobotConfig(string name, map<string, float> &config);

    void setRobotPosition(string name, Offset* pos);
    Offset* getRobotPosition(string name);

    map<string, float> getRobotConfig(string name);
    Animation* getRobotAnimator(string name);


    void print();


};


#endif //COURSEWORK2_REGISTER_H
