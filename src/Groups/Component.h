//
// Created by milo on 13/12/2020.
//

#ifndef COURSEWORK2_COMPONENT_H
#define COURSEWORK2_COMPONENT_H

#include <list>
#include "../3D_Shapes/Shape.h"
#include "../InterfaceController.h"


using namespace std;

enum class StartFace { POS_X, POS_Y, POS_Z, NEG_X, NEG_Y, NEG_Z };
enum class AlignmentY {
    Bot_Bot, Top_Bot, Cen_Bot,
    Bot_Cen, Top_Cen, Cen_Cen,
    Bot_Top, Top_Top, Cen_Top, None
};

enum class AlignmentX {
    Right_Right, Left_Right, Cen_Right,
    Right_Cen, Left_Cen, Cen_Cen,
    Right_Left, Left_Left, Cen_Left, None
};

class Component: public Shape {

public:
    Component(QWidget *parent, string name);
    Component(QWidget *parent,string name, float x_width, float y_height, float z_depth );
    ~Component();

    virtual void initialise() = 0;
    virtual void initTranformation();
    void alignmentTransform(Component *p);
    void appendChildComponent(Component *c);
    void draw();
    void set_isShadow(bool isShadow);

    const string &getName() const;
    void setName(const string &name);

    const map<string, float> &getConfiguration() const;
    void setConfiguration(const map<string, float> &configuration);
    const list<Shape *> &getObjects() const;
    void setObjects(const list<Shape *> &objects);
    const list<Component *> &getChildren() const;
    void setChildren(const list<Component *> &children);

protected:
public:
    void setIsBright(bool isBright);

protected:
    list<Component*> children;
    list<Shape*> objects;
    Component *parentComponent = nullptr;
    Component *root;
    InterfaceController *interface;
    bool isBright = false;
public:
    AlignmentY getAlignmentY() const;

    void setAlignmentY(AlignmentY alignmentY);

protected:
    string name;
    map<string, float> configuration ;
    StartFace startFace = StartFace::POS_Y;
    AlignmentY alignmentY = AlignmentY::None;
    AlignmentX alignmentX = AlignmentX::None;
public:
    InterfaceController *getAnInterface() const;

    void setAnInterface(InterfaceController *anInterface);

public:
    AlignmentX getAlignmentX() const;

    void setAlignmentX(AlignmentX alignmentX);

    StartFace getStartFace() const;

    void setStartFace(StartFace startFace);
};


#endif //COURSEWORK2_COMPONENT_H
