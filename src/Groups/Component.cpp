//
// Created by milo on 13/12/2020.
//

#include <iostream>
#include "Component.h"

Component::~Component(){
    for(Shape *j : this->objects){
        delete j;
    }
    for(Component *j : this->children){
        delete j;
    }
}

Component::Component(QWidget *parent, string name):Shape(parent) {

    this->parentComponent = dynamic_cast<Component*>(parent);
    this->interface = dynamic_cast<InterfaceController*>(parent);
    this->name = name;
    try {
        this->configuration = this->interface->getReg()->getRobotConfig(this->name);
    }catch (...) {
        // Block of code to handle errors
    }

        this->offset = this->interface->getReg()->getRobotPosition(this->name);
    if(this->parentComponent)this->isShadow = this->parentComponent->isShadow;
    if(this->parentComponent)this->root = this->parentComponent->root;
}

Component::Component(QWidget *parent, string name, float x_width, float y_height, float z_depth )
        : Shape(parent, x_width, y_height, z_depth){ // constructor

    this->parentComponent = dynamic_cast<Component*>(parent);
    this->name = name;
    if(this->parentComponent)this->isShadow = this->parentComponent->isShadow;
    if(this->parentComponent)this->root = this->parentComponent->root;
    this->isBright = root->isBright;

} // constructor

void Component::alignmentTransform(Component *p) {

    switch(this->alignmentY){
        case AlignmentY::Bot_Bot: cout << "alignment not implimented 1" << endl; throw 2;
        case AlignmentY::Top_Bot: cout << "alignment not implimented 2" << endl; throw 2;
        case AlignmentY::Cen_Bot: cout << "alignment not implimented 3" << endl; throw 2;
        case AlignmentY::Bot_Cen: cout << "alignment not implimented 4" << endl; throw 2;
        case AlignmentY::Top_Cen: cout << "alignment not implimented 5" << endl; throw 2;
        case AlignmentY::Cen_Cen: cout << "alignment not implimented 6" << endl; throw 2;
        case AlignmentY::Bot_Top:
            glTranslatef(0,p->y_height,0);
            break;
        case AlignmentY::Top_Top: cout << "alignment not implimented 8" << endl; throw 2;
        case AlignmentY::Cen_Top: cout << "alignment not implimented 9" << endl; throw 2;
        case AlignmentY::None: break ;
    }

    switch(this->alignmentX){
        case AlignmentX::Right_Right:
            glTranslatef((p->x_width/2)-(this->x_width/2),0,0);
            break;
        case AlignmentX::Left_Right: cout << "alignment not implimented 1" << endl; throw 2;
        case AlignmentX::Cen_Right: cout << "alignment not implimented 1" << endl; throw 2;
        case AlignmentX::Right_Cen: cout << "alignment not implimented 1" << endl; throw 2;
        case AlignmentX::Left_Cen: cout << "alignment not implimented 1" << endl; throw 2;
        case AlignmentX::Cen_Cen: cout << "alignment not implimented 1" << endl; throw 2;
        case AlignmentX::Right_Left: cout << "alignment not implimented 1" << endl; throw 2;
        case AlignmentX::Left_Left:
            glTranslatef((-p->x_width/2)+(this->x_width/2),0,0);
            break;
        case AlignmentX::Cen_Left: cout << "alignment not implimented 1" << endl; throw 2;
        case AlignmentX::None: break ;
    }
}

void Component::initTranformation(){

    Component *p = this->parentComponent;

    if(p){
        switch(this->startFace){
            case StartFace::POS_X: glTranslatef((p->x_width/2)+(this->x_width/2),0,0); break;
            case StartFace::POS_Y: glTranslatef(0,p->y_height,0); break;
            case StartFace::POS_Z: glTranslatef(0,0,p->z_depth); break;
            case StartFace::NEG_X: glTranslatef((-p->x_width/2)-(this->x_width/2),0,0); break;
            case StartFace::NEG_Y: break;
            case StartFace::NEG_Z: glTranslatef(0,0,-p->z_depth);break;
        }
        this->alignmentTransform(p);
    }

    glTranslatef(this->offset->offset[0],this->offset->offset[1],this->offset->offset[2]);

    switch(this->axis)
    {
        case Xa  : glRotatef(this->angle, 1,0,0); break;
        case Ya: glRotatef(this->angle, 0,1,0); break;
        case Za : glRotatef(this->angle, 0,0,1); break;
    }
}

void Component::draw(){


    glPushMatrix();
    for(Shape *j : this->objects){
        j->initTranformation();
        if(isBright){
            glDisable(GL_LIGHTING);
            glColor3f(1.0,1.0,1.0);
        }
        j->draw();
    }
    glPopMatrix();

    for(Component *j : this->children){
        glPushMatrix();
        j->initialise();
        j->initTranformation();
        j->draw();
        glPopMatrix();
    }
}

void Component::appendChildComponent(Component *c){
    this->children.push_back(c);
}


const list<Component *> &Component::getChildren() const {
    return children;
}

void Component::setChildren(const list<Component *> &children) {
    Component::children = children;
}

const list<Shape *> &Component::getObjects() const {
    return objects;
}

void Component::setObjects(const list<Shape *> &objects) {
    Component::objects = objects;
}


void Component::set_isShadow(bool isShadow) {
    this->isShadow = isShadow;
    for(Shape *j : this->objects){
        j->set_isShadow(isShadow);
    }
    for(Component *j : this->children){
        j->set_isShadow(isShadow);
    }
}


const string &Component::getName() const {
    return name;
}

void Component::setName(const string &name) {
    Component::name = name;
}

const map<string, float> &Component::getConfiguration() const {
    return configuration;
}

void Component::setConfiguration(const map<string, float> &configuration) {
    Component::configuration = configuration;
}

StartFace Component::getStartFace() const {
    return startFace;
}

void Component::setStartFace(StartFace startFace) {
    Component::startFace = startFace;
}

AlignmentY Component::getAlignmentY() const {
    return alignmentY;
}

void Component::setAlignmentY(AlignmentY alignmentY) {
    Component::alignmentY = alignmentY;
}

AlignmentX Component::getAlignmentX() const {
    return alignmentX;
}

void Component::setAlignmentX(AlignmentX alignmentX) {
    Component::alignmentX = alignmentX;
}

InterfaceController *Component::getAnInterface() const {
    return interface;
}

void Component::setAnInterface(InterfaceController *anInterface) {
    interface = anInterface;
}

void Component::setIsBright(bool isBright) {
    Component::isBright = isBright;
}







