//
// Created by milo on 21/12/2020.
//

#include "HumanUpperArm.h"
#include "../LowerArm/HumanLowerArm.h"
#include "../../../../../3D_Shapes/Textures/Textures.h"

HumanUpperArm::HumanUpperArm(QWidget *parent, string name, float x_width, float y_height, float z_depth ):
        Component(parent,name, x_width, y_height, z_depth){
}

void HumanUpperArm::initialise(){

    Cube *cube = new Cube(nullptr, 0.3, 1.5, 0.3);
    cube->set_isShadow(this->isShadow);
    cube->setTextureId(Textures::repeatMaterialTextures[1]);
    cube->setRp({1.,2.0});

    this->objects.push_back(cube);


}


