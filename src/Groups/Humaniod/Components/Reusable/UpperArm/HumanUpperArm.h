//
// Created by milo on 21/12/2020.
//

#ifndef COURSEWORK2_HUMANUPPERARM_H
#define COURSEWORK2_HUMANUPPERARM_H


#include "../../../../Component.h"

class HumanUpperArm : public Component {

public:
    HumanUpperArm(QWidget *parent,
                  string name,
                  float x_width,
                  float y_height,
                  float z_depth
    );

    virtual void initialise();


};


#endif //COURSEWORK2_HUMANUPPERARM_H
