//
// Created by milo on 21/12/2020.
//

#ifndef COURSEWORK2_HUMANLOWERARM_H
#define COURSEWORK2_HUMANLOWERARM_H


#include "../../../../Component.h"

class HumanLowerArm : public Component {

public:
    HumanLowerArm(QWidget *parent,
                  string name,
                  float x_width,
                  float y_height,
                  float z_depth
    );

    virtual void initialise();
};


#endif //COURSEWORK2_HUMANLOWERARM_H
