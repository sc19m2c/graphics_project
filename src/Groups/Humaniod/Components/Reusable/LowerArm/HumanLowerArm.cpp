//
// Created by milo on 21/12/2020.
//

#include "HumanLowerArm.h"
#include "../../../../../3D_Shapes/Textures/Textures.h"

HumanLowerArm::HumanLowerArm(QWidget *parent, string name, float x_width, float y_height, float z_depth ):
        Component(parent,name, x_width, y_height, z_depth){
}

void HumanLowerArm::initialise(){

    Cube *cube = new Cube(nullptr, 0.2, 1.0, 0.2);
    cube->set_isShadow(this->isShadow);
    cube->setTextureId(Textures::repeatMaterialTextures[2]);
    cube->setRp({1.0,2.0});
    this->objects.push_back(cube);

}