//
// Created by milo on 21/12/2020.
//

#include "HumanUpperLeg.h"
#include "../../../../../3D_Shapes/Textures/Textures.h"

HumanUpperLeg::HumanUpperLeg(QWidget *parent, string name, float x_width, float y_height, float z_depth ):
        Component(parent,name, x_width, y_height, z_depth){
}

void HumanUpperLeg::initialise(){

    Cube *cube = new Cube(nullptr, 0.4, 1.25, 0.4);
    cube->set_isShadow(this->isShadow);
    cube->setTextureId(Textures::repeatMaterialTextures[1]);
    cube->setRp({1.5,3.0});
    this->objects.push_back(cube);

}