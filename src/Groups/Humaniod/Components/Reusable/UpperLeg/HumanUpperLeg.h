//
// Created by milo on 21/12/2020.
//

#ifndef COURSEWORK2_HUMANUPPERLEG_H
#define COURSEWORK2_HUMANUPPERLEG_H


#include "../../../../Component.h"

class HumanUpperLeg : public Component {

public:
    HumanUpperLeg(QWidget *parent,
                  string name,
                  float x_width,
                  float y_height,
                  float z_depth
    );

    virtual void initialise();


};


#endif //COURSEWORK2_HUMANUPPERLEG_H
