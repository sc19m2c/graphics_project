//
// Created by milo on 22/12/2020.
//

#ifndef COURSEWORK2_HUMANFOOT_H
#define COURSEWORK2_HUMANFOOT_H


#include "../../../../Component.h"

class HumanFoot : public Component {

public:
    HumanFoot(QWidget *parent,
              string name,
              float x_width,
              float y_height,
              float z_depth
    );

    virtual void initialise();


};


#endif //COURSEWORK2_HUMANFOOT_H
