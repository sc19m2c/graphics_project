//
// Created by milo on 22/12/2020.
//

#include "HumanFoot.h"
#include "../../../../../3D_Shapes/Textures/Textures.h"

HumanFoot::HumanFoot(QWidget *parent, string name, float x_width, float y_height, float z_depth ):
        Component(parent,name, x_width, y_height, z_depth){
}

void HumanFoot::initialise(){

    Cube *cube = new Cube(nullptr, 0.25, 0.4, 0.25);
    cube->set_isShadow(this->isShadow);
    cube->setTextureId(Textures::repeatMaterialTextures[0]);
    cube->setRp({1.0,1.0});
    this->objects.push_back(cube);

}