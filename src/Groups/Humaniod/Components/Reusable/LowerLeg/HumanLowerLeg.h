//
// Created by milo on 21/12/2020.
//

#ifndef COURSEWORK2_HUMANLOWERLEG_H
#define COURSEWORK2_HUMANLOWERLEG_H


#include "../../../../Component.h"

class HumanLowerLeg : public Component {

public:
    HumanLowerLeg(QWidget *parent,
                  string name,
                  float x_width,
                  float y_height,
                  float z_depth
    );

    virtual void initialise();


};


#endif //COURSEWORK2_HUMANLOWERLEG_H
