//
// Created by milo on 21/12/2020.
//

#include "HumanLowerLeg.h"
#include "../../../../../3D_Shapes/Textures/Textures.h"

HumanLowerLeg::HumanLowerLeg(QWidget *parent, string name, float x_width, float y_height, float z_depth ):
        Component(parent,name, x_width, y_height, z_depth){
}

void HumanLowerLeg::initialise(){

    Cube *cube = new Cube(nullptr, 0.3, 1.0, 0.3);
    cube->set_isShadow(this->isShadow);
    cube->setTextureId(Textures::repeatMaterialTextures[2]);
    cube->setRp({1.0,2.5});
    this->objects.push_back(cube);

}