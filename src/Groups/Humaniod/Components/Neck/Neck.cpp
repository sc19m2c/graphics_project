//
// Created by milo on 21/12/2020.
//

#include "Neck.h"
#include "../../../../3D_Shapes/Cylinder.h"
#include "../Head/Head.h"
#include "../../../../3D_Shapes/Textures/Textures.h"

Neck::Neck(QWidget *parent,string name, float x_width, float y_height, float z_depth ):
        Component(parent,name, x_width, y_height, z_depth){
}

void Neck::initialise(){

    //need to set the axis so the head can turn
    Cylinder *cylinder = new Cylinder(nullptr, 0.5, 0.4, 0.5);
    cylinder->set_isShadow(this->isShadow);
    cylinder->setTextureId(Textures::repeatMaterialTextures[7]);
    this->objects.push_back(cylinder);

    Head *head = new Head(this, "head", 1.0, 1.0, 1.0);
    head->setAngle(this->root->getConfiguration().at(head->getName()));

    this->children.push_back(head);

}