//
// Created by milo on 21/12/2020.
//

#ifndef COURSEWORK2_BODY_H
#define COURSEWORK2_BODY_H


#include <QWidget>
#include "../../../Component.h"

class Body : public Component{

public:
    Body(QWidget *parent, string name, float x_width, float y_height, float z_depth );
    virtual void initialise();
};


#endif //COURSEWORK2_BODY_H
