//
// Created by milo on 21/12/2020.
//

#include "Body.h"
#include "../Neck/Neck.h"
#include "../Reusable/UpperArm/HumanUpperArm.h"
#include "../Reusable/LowerArm/HumanLowerArm.h"
#include "../Reusable/UpperLeg/HumanUpperLeg.h"
#include "../Reusable/Foot/HumanFoot.h"
#include "../Reusable/LowerLeg/HumanLowerLeg.h"
#include "../../../../3D_Shapes/Textures/Textures.h"

Body::Body(QWidget *parent,string name, float x_width, float y_height, float z_depth ):
        Component(parent,name, x_width, y_height, z_depth){
}

void Body::initialise(){

    Cube *base = new Cube(nullptr, 1.5,1.5,0.5);
    base->set_isShadow(this->isShadow);
    base->setTextureId(Textures::repeatMaterialTextures[0]);
    base->setRp({2.0,4.0});
    this->objects.push_back(base);


    /*
     * Neck
     * lA_U
     * RA_U
     * LL_U
     * RL_U
     * */

    Neck *neck = new Neck(this, "neck", 0.5, 0.4, 0.5);
    neck->setAngle(this->root->getConfiguration().at(neck->getName()));
    neck->setAxis(Ya);

    // ---------------- Arm ------------- //

    HumanUpperArm *leftUpperArm = new HumanUpperArm(this, "left_upper_arm", 0.3, 1.5, 0.3);
    leftUpperArm->setStartFace(StartFace::NEG_X);
    leftUpperArm->setAlignmentY(AlignmentY::Bot_Top);
    leftUpperArm->setAngle(this->root->getConfiguration().at(leftUpperArm->getName()));
    leftUpperArm->setAxis(Xa);

    HumanLowerArm *leftLowerArm = new HumanLowerArm(leftUpperArm, "left_lower_arm", 0.4, 0.75, 0.4);
    leftLowerArm->setAngle(this->root->getConfiguration().at(leftLowerArm->getName()));
    leftLowerArm->setAxis(Xa);
    leftUpperArm->appendChildComponent(leftLowerArm);

    HumanUpperArm *rightUpperArm = new HumanUpperArm(this, "right_upper_arm", 0.3, 1.5, 0.3);
    rightUpperArm->setStartFace(StartFace::POS_X);
    rightUpperArm->setAlignmentY(AlignmentY::Bot_Top);
    rightUpperArm->setAngle(this->root->getConfiguration().at(rightUpperArm->getName()));
    rightUpperArm->setAxis(Xa);

    HumanLowerArm *rightLowerArm = new HumanLowerArm(rightUpperArm, "right_lower_arm", 0.4, 0.75, 0.4);
    rightLowerArm->setAngle(this->root->getConfiguration().at(rightLowerArm->getName()));
    rightLowerArm->setAxis(Xa);
    rightUpperArm->appendChildComponent(rightLowerArm);

    // ---------------- Legs ----------------- //

    HumanUpperLeg *leftUpperLeg = new HumanUpperLeg(this, "left_upper_leg", 0.4, 1.25, 0.4);
    leftUpperLeg->setStartFace(StartFace::NEG_Y);
    leftUpperLeg->setAlignmentX(AlignmentX::Left_Left);
    leftUpperLeg->setAngle(this->root->getConfiguration().at(leftUpperLeg->getName()));
    leftUpperLeg->setAxis(Xa);

    HumanLowerLeg *leftLowerLeg = new HumanLowerLeg(leftUpperLeg, "left_lower_leg", 0.3, 1.0, 0.3);
    leftLowerLeg->setAngle(this->root->getConfiguration().at(leftLowerLeg->getName()));
    leftLowerLeg->setAxis(Xa);
    leftUpperLeg->appendChildComponent(leftLowerLeg);

    HumanFoot *leftFoot = new HumanFoot(leftLowerLeg, "left_foot", 0.25, 0.4, 0.25);
    leftFoot->setAngle(this->root->getConfiguration().at(leftFoot->getName()));
    leftFoot->setAxis(Xa);
    leftLowerLeg->appendChildComponent(leftFoot);

    HumanUpperLeg *rightUpperLeg = new HumanUpperLeg(this, "right_upper_leg", 0.4, 1.25, 0.4);
    rightUpperLeg->setStartFace(StartFace::NEG_Y);
    rightUpperLeg->setAlignmentX(AlignmentX::Right_Right);
    rightUpperLeg->setAngle(this->root->getConfiguration().at(rightUpperLeg->getName()));
    rightUpperLeg->setAxis(Xa);

    HumanLowerLeg *rightLowerLeg = new HumanLowerLeg(rightUpperLeg, "right_lower_leg", 0.3, 1.0, 0.3);
    rightLowerLeg->setAngle(this->root->getConfiguration().at(rightLowerLeg->getName()));
    rightLowerLeg->setAxis(Xa);
    rightUpperLeg->appendChildComponent(rightLowerLeg);

    HumanFoot *rightFoot = new HumanFoot(rightLowerLeg, "right_foot", 0.25, 0.4, 0.25);
    rightFoot->setAngle(this->root->getConfiguration().at(rightFoot->getName()));
    rightFoot->setAxis(Xa);
    rightLowerLeg->appendChildComponent(rightFoot);


    this->children.push_back(neck);
    this->children.push_back(leftUpperArm);
    this->children.push_back(rightUpperArm);
    this->children.push_back(leftUpperLeg);
    this->children.push_back(rightUpperLeg);


}