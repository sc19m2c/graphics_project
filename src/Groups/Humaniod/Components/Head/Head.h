//
// Created by milo on 21/12/2020.
//

#ifndef COURSEWORK2_HEAD_H
#define COURSEWORK2_HEAD_H


#include "../../../Component.h"

class Head : public Component {

public:
    Head(QWidget
         *parent,
         string name,
         float x_width,
         float y_height,
         float z_depth
    );

    virtual void initialise();


};


#endif //COURSEWORK2_HEAD_H
