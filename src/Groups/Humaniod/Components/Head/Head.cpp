//
// Created by milo on 21/12/2020.
//

#include "Head.h"
#include "../../../../3D_Shapes/Sphere.h"
#include "../../../../3D_Shapes/Textures/Textures.h"

Head::Head(QWidget *parent,string name, float x_width, float y_height, float z_depth ):
        Component(parent,name, x_width, y_height, z_depth){
}

void headSpotRotation(){
    glRotatef(90,1,0,0);
}

void Head::initialise(){


    Sphere *sphere = new Sphere(nullptr, 1.0, 1.0, 1.0);
    sphere->set_isShadow(this->isShadow);
    sphere->setTextureId(Textures::flatMaterialTextures[1]);
    sphere->setSpotRotation(headSpotRotation);
    sphere->setAxis(Ya);
    sphere->setAngle(90);

    this->objects.push_back(sphere);

}