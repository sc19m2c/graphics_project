//
// Created by milo on 21/12/2020.
//

#include "Humanoid.h"
#include "Components/Body/Body.h"
#include "Components/Neck/Neck.h"


Humanoid::Humanoid(QWidget *parent, string name): Component(parent, name){
    this->root = this;
    this->y_height = 0;
    this->x_width = 0;
    this->z_depth = 0;

}

void Humanoid::initialise(){

    Body *base = new Body(this,"root", 1.5,1.5,0.5);
    base->setAngle(this->configuration.at(base->getName()));
    base->setAxis(Ya);
    this->children.push_back(base);




//    this->siblings.push_back(rightArm);
}