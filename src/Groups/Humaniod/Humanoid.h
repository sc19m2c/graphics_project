//
// Created by milo on 21/12/2020.
//

#ifndef COURSEWORK2_HUMANOID_H
#define COURSEWORK2_HUMANOID_H


#include "../Component.h"

class Humanoid : public Component{

public:
    Humanoid(QWidget *parent, string name);
    virtual void initialise();
};


#endif //COURSEWORK2_HUMANOID_H
