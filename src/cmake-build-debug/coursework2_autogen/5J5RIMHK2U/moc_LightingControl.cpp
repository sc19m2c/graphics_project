/****************************************************************************
** Meta object code from reading C++ file 'LightingControl.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../UI/ControlPanel/LightingControl/LightingControl.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'LightingControl.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_LightingControl_t {
    QByteArrayData data[10];
    char stringdata0[121];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LightingControl_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LightingControl_t qt_meta_stringdata_LightingControl = {
    {
QT_MOC_LITERAL(0, 0, 15), // "LightingControl"
QT_MOC_LITERAL(1, 16, 13), // "setIsTextures"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 10), // "isTextures"
QT_MOC_LITERAL(4, 42, 12), // "setIsShadows"
QT_MOC_LITERAL(5, 55, 12), // "setIsClipped"
QT_MOC_LITERAL(6, 68, 12), // "setIsCubemap"
QT_MOC_LITERAL(7, 81, 13), // "setIsWordlmap"
QT_MOC_LITERAL(8, 95, 13), // "setIsHumanoid"
QT_MOC_LITERAL(9, 109, 11) // "setIsCenter"

    },
    "LightingControl\0setIsTextures\0\0"
    "isTextures\0setIsShadows\0setIsClipped\0"
    "setIsCubemap\0setIsWordlmap\0setIsHumanoid\0"
    "setIsCenter"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LightingControl[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   49,    2, 0x0a /* Public */,
       4,    1,   52,    2, 0x0a /* Public */,
       5,    1,   55,    2, 0x0a /* Public */,
       6,    1,   58,    2, 0x0a /* Public */,
       7,    1,   61,    2, 0x0a /* Public */,
       8,    1,   64,    2, 0x0a /* Public */,
       9,    1,   67,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,

       0        // eod
};

void LightingControl::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<LightingControl *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->setIsTextures((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->setIsShadows((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->setIsClipped((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->setIsCubemap((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->setIsWordlmap((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->setIsHumanoid((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->setIsCenter((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject LightingControl::staticMetaObject = { {
    &MasterLayout::staticMetaObject,
    qt_meta_stringdata_LightingControl.data,
    qt_meta_data_LightingControl,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *LightingControl::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LightingControl::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_LightingControl.stringdata0))
        return static_cast<void*>(this);
    return MasterLayout::qt_metacast(_clname);
}

int LightingControl::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = MasterLayout::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
